<?php

namespace App;
use App\Comments;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded = [];


    public function comments()
    {
        return $this->hasMany(Comments::class);
    }
}
