<?php

namespace App;
use App\Category;
use App\Jobs;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

public function category(){

    return $this->belongsTo(Category::class);
}

public function job()
{
    return $this->hasMany(Jobs::class);
}

}
