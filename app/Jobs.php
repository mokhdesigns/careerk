<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Company;

class Jobs extends Model
{

    protected $guarded = [];


    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
