<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Jobs;
class Apply extends Model
{
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function job()
    {
        return $this->belongsTo(Jobs::class);
    }


}
