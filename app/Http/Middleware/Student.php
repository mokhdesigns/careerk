<?php

namespace App\Http\Middleware;

use Closure;

class Student
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user->role->id === 2){

            return $next($request);
        }

        return redirect('/login')
        ->with(['error' => "You do not have the permission to enter this site. Please login with correct user."]);
    }
}
