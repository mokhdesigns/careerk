<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Apply;
use Auth;
use App\Jobs;

class ApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'job_id'     => 'required|integer',
        ]);

        $data = $request->all();

        $data['user_id'] = Auth::user()->id;

        $exist = Apply::where([
            ['job_id', '=', $request->job_id],
            ['user_id', '=', $data['user_id']]
            
            ])->count();

        if($exist > 0){

            
       session()->flash('message', 'Job Already Applied');

       return redirect('job');

        }else{

        Apply::create($data);

        session()->flash('message', 'You Applied For The Job');

        return redirect('job');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


public function search(Request $request) {

   $location = $request->search;

   $jobs = Jobs::where('title', '=',  $location);

  return view('website.search', compact('jobs'));




//   if(!empty($request->search)){
//     $searchFields = ['title','content','author_name','category_name'];
//     $query->where(function($query) use($request, $searchFields){
//       $searchWildcard = '%' . $request->search . '%';
//       foreach($searchFields as $field){
//         $query->orWhere($field, 'LIKE', $searchWildcard);
//       }
//     });
//   }




}

}
