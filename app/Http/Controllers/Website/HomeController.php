<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\Company;
use App\Category;
use App\Blog;
use App\User;
use App\Notifications\newComment;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $companies = Company::latest()->get();

        $categories = Category::latest()->take(4)->get();


        $categoryDrop = Category::latest()->get();

        $jobs = Jobs::latest()->take(8)->get();

        $blogs = Blog::latest()->take(3)->get();


        return view('website.index', compact('companies', 'jobs', 'categories', 'blogs', 'categoryDrop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function about () {

        $blogs = BLog::latest()->take(3)->get();

        $jobCount = Jobs::count();

        $studentCount = User::where('role_id', '2')->count();

        $companyCount = Company::count();

        $categoryCount = Category::count();

        return view('website.about', compact('blogs', 'jobCount', 'studentCount', 'companyCount', 'categoryCount'));
    }


}
