<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Apply;
use Illuminate\Support\Facades\Hash;
class ProfileController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User()->id;

        $applied  = Apply::where('user_id', '=', $user)
        ->join('jobs', 'jobs.id', '=', 'applies.job_id')
        ->get();

        $appliedCount  = Apply::where('user_id', '=', $user)
        ->join('jobs', 'jobs.id', '=', 'applies.job_id')
        ->count();
        return view('website.profile.index', compact('user', 'applied', 'appliedCount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);

        $request->validate([

            'password'         => 'required|string | min:8 | confirmed',
            'old_password'     => 'required',

        ]);

        if(Hash::check($request->old_password, $user->password)){

        $user->update([

            'password' => bcrypt($request->password)
        ]);

       session()->flash('message', 'Profile Updated Successfully');

       return redirect('profile');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail(Auth::user()->id );

        return view('website.profile.changepassword', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userid = Auth::User()->id;

        $user = User::where('id', '=' , $userid)->first();


        return view('website.profile.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $userid = Auth::User()->id;

        $user = User::where('id', '=' , $userid)->first();




        $request->validate([

            'name'         => 'required|string | min:5',
            'phone'        => 'required|string',
            'experience'   => 'required|string',
            'body'         => 'required|string',
            'email'        => 'required| email',
            'education'    => 'required',
            'public'       => 'required'

        ]);


        $input = $request->all();



        $file = $request->file('avatar');

        $cv = $request->file('cv');

       if($file){

        $fileName = $file->getClientOriginalName();

        $fileName = uniqid() . $fileName;

        $file->move('images/', $fileName);

        $input['avatar'] = $fileName;

       }else {

        $input['avatar'] = $user->avatar;

       }



       if($cv){

        $cvName = $cv->getClientOriginalName();

        $cvName = uniqid() . $cvName;

        $cv->move('images/', $cvName);

        $input['cv'] = $cvName;

       }else {

        $input['cv'] = $user->cv;

       }

       $input['public'] = $request->public;


       $user->update($input);


       session()->flash('message', 'Profile Updated Successfully');

       return redirect('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changPassword() {

        dd('this is right');
    }
}
