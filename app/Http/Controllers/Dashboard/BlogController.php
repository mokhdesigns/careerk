<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::latest()->get();

        return view('admin.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'title'     => 'required|string | min:5',
            'avatar'    => 'required',
            'body'      => 'required|min:50|string',
        ]);


        $input = $request->all();

        $file = $request->file('avatar');

       if($file){

            $fileName = $file->getClientOriginalName();

            $fileName = uniqid() . $fileName;

            $file->move('images/', $fileName);

            $input['avatar'] = $fileName;

       }

       $blog_data = $request->except(['avatar']);

       $blog_data['avatar'] = $fileName;

       $blog = Blog::create([

        'title'   => $blog_data['title'],
        'avatar'  => $blog_data['avatar'],
        'body'    => $blog_data['body']


       ]);

       session()->flash('message', 'Blog Added Successfully');

       return redirect('dashboard/blog');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $blog = Blog::findOrFail($id);

        return view('admin.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);

        $request->validate([

            'title'     => 'required|string | min:5',
            'body'      => 'required|min:50|string',
        ]);


        $input = $request->all();

        $file = $request->file('avatar');

       if($file){

        $fileName = $file->getClientOriginalName();

        $fileName = uniqid() . $fileName;

        $file->move('images/', $fileName);

        $input['avatar'] = $fileName;

       }else {

        $input['avatar'] = $blog->avatar;

       }

       $blog_data = $request->except(['avatar']);

       $blog_data['avatar'] = $input['avatar'];

       $blog->update($blog_data);

       session()->flash('message', 'Blog Updates Successfully');

       return redirect('dashboard/blog');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BLog::findOrFail($id)->delete();

        session()->flash('message', 'Blog Deleted Successfully');

        return redirect('dashboard/blog');
    }
}
