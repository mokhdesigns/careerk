<?php

namespace App\Http\Controllers\Dashboard;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\Company;
use App\Category;
use App\Blog;
use App\Contact;
use App\Apply;
use DB;
use Auth;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('Admin');
    }


    public function index()
    {
        $userCount = User::count();

        $blogCount = Blog::count();

        $jobCount = Jobs::count();

        $CompanyCount = Company::count();

        $mailCount = Contact::where('viewed', '=', '0')->count();

        $recentUsers = User::where('role_id', '=', '2')->latest()->take('6')->get();

        $jobschart= Jobs::select(DB::raw("SUM(id) as count"))

        ->orderBy("created_at")

        ->groupBy(DB::raw("month(created_at)"))

        ->get()->toArray();

        $jobschart = array_column($jobschart, 'count');


        $studentChart = User::select(DB::raw("SUM(id) as count"))
        ->where('role_id', '=', '2')
        ->orderBy("created_at")
        ->groupBy(DB::raw("month(created_at)"))
        ->get()->toArray();

        $studentChart = array_column($studentChart, 'count');

        $applyChart = Apply::select(DB::raw("SUM(id) as count"))
        ->orderBy("created_at")
        ->groupBy(DB::raw("month(created_at)"))
        ->get()->toArray();

        $applyChart = array_column($applyChart, 'count');

        $blogChart = Blog::select(DB::raw("SUM(id) as count"))
        ->orderBy("created_at")
        ->groupBy(DB::raw("month(created_at)"))
        ->get()->toArray();

        $blogChart = array_column($blogChart, 'count');

        return view('admin.index', compact('mailCount', 'recentUsers', 'userCount', 'CompanyCount', 'jobCount', 'blogCount'))->with('jobschart',json_encode($jobschart,JSON_NUMERIC_CHECK))
        ->with('studentChart',json_encode($studentChart,JSON_NUMERIC_CHECK))
        ->with('applyChart',json_encode($applyChart,JSON_NUMERIC_CHECK))
        ->with('blogChart',json_encode($blogChart,JSON_NUMERIC_CHECK));
    }




    public function students()
    {
        $students = User::where('role_id', '2')->get();

        return view('admin.students', compact('students'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
