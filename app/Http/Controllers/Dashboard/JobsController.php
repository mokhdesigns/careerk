<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs;
use App\Category;
use App\Company;

class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Jobs::latest()->get();

        return view('admin.jobs.index', compact('jobs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::latest()->get();

        $companies = Company::latest()->get();

        return view('admin.jobs.create', compact('categories', 'companies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'title'      => 'required|string | min:5',
            'position'   => 'required|string',
            'experience' => 'required|string',
            'avatar'     => 'required',
            'body'       => 'required|min:50|string'

        ]);


        $input = $request->all();

        $file = $request->file('avatar');

       if($file){

        $fileName = $file->getClientOriginalName();

        $fileName = uniqid() . $fileName;

        $file->move('images/', $fileName);

        $input['avatar'] = $fileName;

       }

       $job_data = $request->except(['avatar']);

       $job_data['avatar'] = $fileName;

       $user = Jobs::create($job_data);

       session()->flash('message', 'Job Added Successfully');

       return redirect('dashboard/jobs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Jobs::findOrFail($id);

        $categories = Category::latest()->get();

        $companies = Company::latest()->get();

        return view('admin.jobs.edit', compact('categories', 'companies', 'job'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     
        $job = Jobs::findOrFail($id);

        $request->validate([

            'title'      => 'required|string | min:5',
            'position'   => 'required|string',
            'experience' => 'required|string',
            'body'       => 'required|min:50|string'
    
        ]);
    

        $input = $request->all();

        $file = $request->file('avatar');

       if($file){
          
        $fileName = $file->getClientOriginalName();

        $fileName = uniqid() . $fileName;

        $file->move('images/', $fileName);
        
        $input['avatar'] = $fileName;

       }else {

        $input['avatar'] = $job->avatar;

       }
       

       $job->update($input);

       session()->flash('message', 'Job Updated Successfully');

       return redirect('dashboard/jobs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jobs::findOrFail($id)->delete();
        
        session()->flash('message', 'Job Deleted Successfully');
 
        return redirect('dashboard/jobs');
    }
}
