<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/';


    public function __construct()
    {
        $this->middleware('guest');
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'            => ['required', 'string', 'max:255'],
            'email'           => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'        => ['required', 'string', 'min:8', 'confirmed'],
            'phone'           => ['required'],
            'education'       => ['required', 'string', 'max:255'],
            'body'            => ['required', 'string', 'max:255'],
            'avatar'          => ['required'],
            'cv'              => ['required'],
            'experience'      => ['required', 'string', 'max:255'],
            'gender'          => ['required'],
            'career'          => ['required', 'string', 'max:255'],


        ]);

        $file = $data->file('avatar');

        if($file){

         $fileName = $file->getClientOriginalName();

         $fileName = uniqid() . $fileName;

         $file->move('images/', $fileName);

         $data['avatar'] = $fileName;

        }



        $cv = $data->file('cv');

         $vcName = $cv->getClientOriginalName();

         $vcExtention = $cv->getClientOriginalExtension();

         $vcName = uniqid() . $vcName;

         $cv->move('images/', $vcName);

         $data['cv'] = $vcName;

    }


    protected function create(array $data)
    {

        if($data['avatar']){

            $fileName = $data['avatar']->getClientOriginalName();

            $fileName = uniqid() . $fileName;

            $data['avatar']->move('images/', $fileName);

            $data['avatar'] = $fileName;

           }

        if($data['cv']){

        $vcName = $data['cv']->getClientOriginalName();

        $vcExtention = $data['cv']->getClientOriginalExtension();

        $vcName = uniqid() . $vcName;

        $data['cv']->move('images/', $vcName);

        if($vcExtention == 'pdf'){

            $data['cv'] = $vcName;

        }else{

            session()->flash('message', 'CV Should Be PDF');

            return redirect('/register');
        }
        }

        return User::create([
            'name'       => $data['name'],
            'email'      => $data['email'],
            'password'   => Hash::make($data['password']),
            'phone'      => $data['phone'],
            'role_id'    => '2',
            'education'  => $data['education'],
            'body'       => $data['body'],
            'cv'         => $data['cv'],
            'avatar'     => $data['avatar'],
            'experience' => $data['experience'],
            'gender'     => $data['gender'],
            'career'     => $data['career'],
            'experience' => $data['experience'],


         ]);
    }
}
