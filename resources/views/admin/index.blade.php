@extends('layouts.admin')
@section('content')
  <div class="content-wrapper">
    <div class="container-fluid">

  <div class="row mt-3">
    <div class="col-12 col-lg-6 col-xl-3">
      <div class="card">
       <div class="card-body">
          <div class="media align-items-center">
            <div class="w-icon"><i class="fa fa-comments-o text-white"></i></div>
            <div class="media-body ml-3 border-left-xs border-light-3">
              <h4 class="mb-0 ml-3">{{ $userCount }}</h4>
              <p class="mb-0 ml-3 extra-small-font">Total Users</p>
            </div>
          </div>
        </div>
      </div>
     </div>

     <div class="col-12 col-lg-6 col-xl-3">
      <div class="card">
       <div class="card-body">
          <div class="media align-items-center">
            <div class="w-icon"><i class="fa fa-question-circle-o text-white"></i></div>
            <div class="media-body ml-3 border-left-xs border-light-3">
              <h4 class="mb-0 ml-3">{{ $blogCount}}</h4>
              <p class="mb-0 ml-3 extra-small-font">Total Blogs</p>
            </div>
          </div>
        </div>
      </div>
     </div>

     <div class="col-12 col-lg-6 col-xl-3">
      <div class="card">
       <div class="card-body">
          <div class="media align-items-center">
            <div class="w-icon"><i class="fa fa-bar-chart text-white"></i></div>
            <div class="media-body ml-3 border-left-xs border-light-3">
              <h4 class="mb-0 ml-3"> {{ $jobCount }}</h4>
              <p class="mb-0 ml-3 extra-small-font">Total Jobs</p>
            </div>
          </div>
        </div>
      </div>
     </div>

     <div class="col-12 col-lg-6 col-xl-3">
      <div class="card">
       <div class="card-body">
          <div class="media align-items-center">
            <div class="w-icon"><i class="fa fa-money text-white"></i></div>
            <div class="media-body ml-3 border-left-xs border-light-3">
              <h4 class="mb-0 ml-3">{{ $CompanyCount}}</h4>
              <p class="mb-0 ml-3 extra-small-font">Total Companies</p>
            </div>
          </div>
        </div>
      </div>
     </div>

   </div><!--End Row-->

    <div class="row">
	   <div class="col-12 col-lg-6 col-xl-6">
		 <div class="card">
		   <div class="card-header">Added Jobs Review Chart
			   <div class="card-action">
				 <div class="dropdown">
				 <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
				  <i class="icon-options"></i>
				 </a>
				  <div class="dropdown-menu dropdown-menu-right">
				  <a class="dropdown-item" href="javascript:void();">Action</a>
				  <a class="dropdown-item" href="javascript:void();">Another action</a>
				  <a class="dropdown-item" href="javascript:void();">Something else here</a>
				  <div class="dropdown-divider"></div>
				  <a class="dropdown-item" href="javascript:void();">Separated link</a>
				  </div>
				</div>
             </div>
		   </div>
		   <div class="card-body">
			 <canvas id="chart1" height="150"></canvas>
		   </div>
		 </div>
	   </div>
	   <div class="col-12 col-lg-6 col-xl-6">
		 <div class="card">
		   <div class="card-header">Registered Students Chart
			 <div class="card-action">
				 <div class="dropdown">
				 <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
				  <i class="icon-options"></i>
				 </a>
				  <div class="dropdown-menu dropdown-menu-right">
				  <a class="dropdown-item" href="javascript:void();">Action</a>
				  <a class="dropdown-item" href="javascript:void();">Another action</a>
				  <a class="dropdown-item" href="javascript:void();">Something else here</a>
				  <div class="dropdown-divider"></div>
				  <a class="dropdown-item" href="javascript:void();">Separated link</a>
				  </div>
				</div>
             </div>
		   </div>
		   <div class="card-body">
			 <canvas id="chart2" height="150"></canvas>
		   </div>
		 </div>
	   </div>
	 </div><!--End Row-->
     <div class="row">
            <div class="col-12 col-lg-6 col-xl-6">
              <div class="card">
                <div class="card-header">Applied Jobs
                    <div class="card-action">
                      <div class="dropdown">
                      <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
                       <i class="icon-options"></i>
                      </a>
                       <div class="dropdown-menu dropdown-menu-right">
                       <a class="dropdown-item" href="javascript:void();">Action</a>
                       <a class="dropdown-item" href="javascript:void();">Another action</a>
                       <a class="dropdown-item" href="javascript:void();">Something else here</a>
                       <div class="dropdown-divider"></div>
                       <a class="dropdown-item" href="javascript:void();">Separated link</a>
                       </div>
                     </div>
                  </div>
                </div>
                <div class="card-body">
                  <canvas id="chart3" height="150"></canvas>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-6">
              <div class="card">
                <div class="card-header">Published Blogs
                </div>
                <div class="card-body">
                  <canvas id="chart4" height="150"></canvas>
                </div>
              </div>
            </div>
          </div><!--End Row-->

   <div class="row">
        <div class="col-12 col-lg-12">
          <div class="card">
            <div class="card-header">Recent Registered Students
            </div>
              <div class="table-responsive">
                    <table class="table align-items-center table-flush table-borderless">
                     <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Photo</th>
                        <th>Date</th>
                      </tr>
                      </thead>
                      <tbody>
                        <tr>
  @forelse ($recentUsers as $recent)

    <td>{{ $recent->name }}</td>
    <td>{{ $recent->email }}</td>
    <td>{{ $recent->phone }}</td>
    <td><img src="{{asset('/images/' . $recent->avatar) }}" class="company_photo" style="border-radius: 50%;width: 60px;height: 60px;"></td>
    <td>{{ date("Y M D", strtotime($recent->created_at)) }}</td>
    </tr>

    @empty

    <h6 class="text-center"> No Students Yet </h6>

    @endforelse
                    </tbody></table>
                  </div>
          </div>
        </div>
       </div>
    </div>
    <!-- End container-fluid-->

   </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->


  </div><!--End wrapper-->



  <script>

        // chart 1

        var ctx = document.getElementById('chart1').getContext('2d');

          var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
                  datasets: [{
                      label: 'jobs',
                      type: 'line',
                      data: <?php echo $jobschart ?> ,
                      backgroundColor: "rgba(255, 255, 255, 0.12)",
                      borderColor: "#fff",
                      pointBackgroundColor:'transparent',
                      pointHoverBackgroundColor:'#fff',
                      pointBorderWidth :3,
                      pointRadius :3,
                      pointHoverRadius :3,
                      borderWidth: 2

                  }]
              },
          options: {
              legend: {
                display: true,
                labels: {
                  fontColor: '#ddd',
                  boxWidth:40
                }
              },
              tooltips: {
                displayColors:false
              },
            scales: {
                xAxes: [{
                  barPercentage: .5,
                  ticks: {
                      beginAtZero:true,
                      fontColor: '#ddd'
                  },
                  gridLines: {
                    display: true ,
                    color: "rgba(221, 221, 221, 0.08)"
                  },
                }],
                 yAxes: [{
                  ticks: {
                      beginAtZero:true,
                      fontColor: '#ddd'
                  },
                  gridLines: {
                    display: true ,
                    color: "rgba(221, 221, 221, 0.08)"
                  },
                }]
               }

           }
          });



// chart 2

var ctx = document.getElementById('chart2').getContext('2d');

var myChart = new Chart(ctx, {
type: 'bar',
data: {
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    datasets: [{
        label: 'Students',
        data: <?php echo $studentChart ?>,
        backgroundColor: '#fff',
        borderColor: "transparent",
        pointRadius :"0",
        lineTension :'0',
        borderWidth: 3
    }]
},
options: {
legend: {
  display: true,
  labels: {
    fontColor: '#ddd',
    boxWidth:40
  }
},
tooltips: {
  displayColors:false
},
scales: {
  xAxes: [{
    ticks: {
        beginAtZero:true,
        fontColor: '#ddd'
    },
    gridLines: {
      display: true ,
      color: "rgba(221, 221, 221, 0.08)"
    },
  }],
   yAxes: [{
    ticks: {
        beginAtZero:true,
        fontColor: '#ddd'
    },
    gridLines: {
      display: true ,
      color: "rgba(221, 221, 221, 0.08)"
    },
  }]
 }

}
});


     // chart 3

     var ctx = document.getElementById('chart3').getContext('2d');

     var myChart = new Chart(ctx, {
         type: 'line',
         data: {
             labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
             datasets: [{
                 label: 'Applied Jobs',
                 data: <?php echo $applyChart ?>,
                 backgroundColor: '#fff',
                 borderColor: "transparent",
                 pointRadius :"0",
                 borderWidth: 3
             }]
         },
     options: {
         legend: {
           display: false,
           labels: {
             fontColor: '#ddd',
             boxWidth:40
           }
         },
         tooltips: {
           displayColors:false
         },
       scales: {
           xAxes: [{
             ticks: {
                 beginAtZero:true,
                 fontColor: '#ddd'
             },
             gridLines: {
               display: true ,
               color: "rgba(221, 221, 221, 0.08)"
             },
           }],
            yAxes: [{
             ticks: {
                 beginAtZero:true,
                 fontColor: '#ddd'
             },
             gridLines: {
               display: true ,
               color: "rgba(221, 221, 221, 0.08)"
             },
           }]
          }

      }
     });

// chart 4

var ctx = document.getElementById('chart4').getContext('2d');

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
        datasets: [{
            label: 'jobs',
            type: 'line',
            data: <?php echo $blogChart ?> ,
            backgroundColor: "rgba(255, 255, 255, 0.12)",
            borderColor: "#fff",
            pointBackgroundColor:'transparent',
            pointHoverBackgroundColor:'#fff',
            pointBorderWidth :3,
            pointRadius :3,
            pointHoverRadius :3,
            borderWidth: 2

        }]
    },
options: {
    legend: {
      display: true,
      labels: {
        fontColor: '#ddd',
        boxWidth:40
      }
    },
    tooltips: {
      displayColors:false
    },
  scales: {
      xAxes: [{
        barPercentage: .5,
        ticks: {
            beginAtZero:true,
            fontColor: '#ddd'
        },
        gridLines: {
          display: true ,
          color: "rgba(221, 221, 221, 0.08)"
        },
      }],
       yAxes: [{
        ticks: {
            beginAtZero:true,
            fontColor: '#ddd'
        },
        gridLines: {
          display: true ,
          color: "rgba(221, 221, 221, 0.08)"
        },
      }]
     }

 }
});
      </script>


@endsection
