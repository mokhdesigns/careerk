@extends('layouts.admin')
@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('dashboard.blog.index')}}">Blogs</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add New Blog</li>
         </ol>
	   </div>
     </div>

    <!-- End Breadcrumb-->

    <div class="row">
        <div class="col-lg-12">

   <div class="card">
       <div class="card-body">
       <div class="card-title">Add New Blog</div>
       <hr>
        <form method="POST" action="{{ route('dashboard.blog.store') }}" enctype="multipart/form-data">

            @csrf

<div class="row">

<div class="col-md-6">
       <div class="form-group row">
        <label for="input-26" class="col-sm-3 col-form-label">Blog Title</label>
        <div class="col-sm-9">
        <input required="required" type="text" value="{{ old('title') }}" class="form-control form-control-rounded @error('title') is-invalid @enderror" id="input-26" placeholder="Enter Blog Title" name="title">
        </div>
      </div>
</div>

 <div class="col-md-6">
        <div class="form-group row">
                <label for="input-30" class="col-sm-3 col-form-label">Blog Photo</label>
                <div class="col-sm-9">
                <input type="file" class="form-control form-control-rounded @error('logo') is-invalid @enderror" id="input-30" required="required" name="avatar">
                </div>
              </div>
 </div>
</div>

        <div class="card-header text-uppercase"> Blog Descreption </div>
        <div class="card-body">
         <textarea name="body" id="summernoteEditor" style="display: none;" class="@error('body') is-invalid @enderror" required="required"></textarea



      <div class="form-group row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <div class="form-footer">
                <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
            </div>
        </div>
      </div>
      </form>
     </div>
     </div>

        </div>
      </div>

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->


    @include('partials._errors')

  </div><!--End wrapper-->


@endsection
