@extends('layouts.admin')
@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Students</h4>
	   </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Avatar</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Education</th>
                        <th>Address</th>
                        <th>Gender</th>
                        <th>Date Of Birth</th>
                        <th>Join Date</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($students as $student)

                    <tr>
                        <td>{{ $student->name }}</td>
                        <td><img src="{{asset('images/'. $student->avatar) }}" class="company_photo"> </td>
                        <td>{{ $student->email}}</td>
                        <td>{{ $student->phone}}</td>
                        <td>{{ $student->education }}</td>
                        <td>{{ $student->Address }} </td>
                        <td>{{ $student->gender }}</td>
                        <td>{{ $student->birth }}</td>
                        <td>{{ $student->created_at }}</td>

                    </tr>

                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                            <th>Name</th>
                            <th>Avatar</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Education</th>
                            <th>Address</th>
                            <th>Gender</th>
                            <th>Date Of Birth</th>
                            <th>Join Date</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    </div>
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
  </div>


@endsection
