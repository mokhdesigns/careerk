@extends('layouts.admin')
@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Jobs</h4>
	   </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Company</th>
                        <th>Category</th>
                        <th>Avatar</th>
                        <th>Position</th>
                        <th>Paid</th>
                        <th>Type</th>
                        <th>Experience</th>
                        <th>Control</th>

                    </tr>
                </thead>
                <tbody>

                    @foreach ($jobs as $job)

                    <tr>
                        <td>{{ $job->title }}</td>
                        <td>{{ $job->company['title'] }}</td>
                        <td>{{ $job->category->title }}</td>
                        <td><img src="{{asset('images/'. $job->avatar) }}" class="company_photo"> </td>
                        <td>{{ $job->position }}</td>
                        <td> @if($job->paid == 1) <span class="badge badge-success m-1">Paid</span> @else <span class="badge badge-danger m-1">Not Paid</span> @endif </td>
                        <td>@if($job->type == 1) <span class="badge badge-success m-1">Full Time</span> @elseif($job->type == 2) <span class="badge badge-warning m-1">Part Time </span> @elseif($job->type == 3) <span class="badge badge-danger m-1">Freelance</span> @elseif($job->type == 4) <span class="badge badge-primary m-1">Internship </span>@endif </td>
                        <td>{{ $job->experience }}</td>
                        <td><div class="btn-group m-1">
                            <form method="post" action="{{ route('dashboard.jobs.destroy', $job->id ) }}">

                                @method('DELETE')
                                @csrf

                                <button class="delete_btn btn btn-light waves-effect waves-light"> <i class="fa fa fa-trash-o"></i> </button>
                             </form>

                            <a type="button" href="{{ route('dashboard.jobs.edit', $job->id)}}" class="btn btn-light waves-effect waves-light"> <i class="fa fa-edit"></i> </a>

                         </div></td>
                    </tr>

                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                            <th>Title</th>
                            <th>Company</th>
                            <th>Category</th>
                            <th>Avatar</th>
                            <th>Position</th>
                            <th>Paid</th>
                            <th>Type</th>
                            <th>Experience</th>
                            <th>Control</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2018 Dashtreme Admin
        </div>
      </div>
    </footer>
	<!--End footer-->

  </div><!--End wrapper-->


@endsection
