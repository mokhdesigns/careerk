@extends('layouts.admin')
@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('dashboard.jobs.index')}}">Jobs</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add New Job</li>
         </ol>
	   </div>
     </div>

    <!-- End Breadcrumb-->

    <div class="row">
        <div class="col-lg-12">

   <div class="card">
       <div class="card-body">
       <div class="card-title">Add New Company</div>
       <hr>
        <form method="POST" action="{{ route('dashboard.jobs.store') }}" enctype="multipart/form-data">

            @csrf

<div class="row">

<div class="col-md-6">
       <div class="form-group row">
        <label for="input-26" class="col-sm-3 col-form-label">Job Title</label>
        <div class="col-sm-9">
        <input required="required" type="text" value="{{ old('title') }}" class="form-control form-control-rounded @error('title') is-invalid @enderror" id="input-26" placeholder="Enter Job Title" name="title">
        </div>
      </div>
</div>

<div class="col-md-6">
      <div class="form-group row">
        <label for="input-27" class="col-sm-3 col-form-label">Company </label>
        <div class="col-sm-9">
        <select class="form-control single-select select2-hidden-accessible form-control-rounded" data-select2-id="1" tabindex="-1" aria-hidden="true" name="company_id">
                @foreach ($companies as $company)
                <option value="{{ $company->id }} ">{{ $company->title }}</option>
                @endforeach

             </select>
        </div>
      </div>
</div>

<div class="col-md-6">
      <div class="form-group row">
        <label for="input-27" class="col-sm-3 col-form-label">Category</label>
        <div class="col-sm-9">
                    <select class="form-control single-select select2-hidden-accessible form-control-rounded" data-select2-id="1" tabindex="-1" aria-hidden="true" name="category_id">
                       @foreach ($categories as $category)
                       <option value="{{ $category->id }} ">{{ $category->title }}</option>
                       @endforeach

                    </select>
                  </div>
      </div>
</div>

<div class="col-md-6">
        <div class="form-group row">
          <label for="input-27" class="col-sm-3 col-form-label">Type</label>
          <div class="col-sm-9">
                      <select class="form-control single-select select2-hidden-accessible form-control-rounded" data-select2-id="1" tabindex="-1" aria-hidden="true" name="type">

                         <option value="1">Full Time</option>
                         <option value="2">Part Time</option>
                         <option value="3">Freelance</option>
                         <option value="4">Internship</option>
                      </select>
                    </div>
        </div>
  </div>

  <div class="col-md-6">
        <div class="form-group row">
          <label for="input-27" class="col-sm-3 col-form-label">finance</label>
          <div class="col-sm-9">
                      <select class="form-control single-select select2-hidden-accessible form-control-rounded" data-select2-id="1" tabindex="-1" aria-hidden="true" name="paid">

                         <option value="1">Paid</option>
                         <option value="0">Not Paid</option>
                      </select>
                    </div>
        </div>
  </div>

  <div class="col-md-6">
        <div class="form-group row">
         <label for="input-26" class="col-sm-3 col-form-label">Position</label>
         <div class="col-sm-9">
         <input required="required" type="text" value="{{ old('position') }}" class="form-control form-control-rounded @error('position') is-invalid @enderror" id="input-26" placeholder="Enter Job Position" name="position">
         </div>
       </div>
 </div>

 <div class="col-md-6">
        <div class="form-group row">
         <label for="input-26" class="col-sm-3 col-form-label">Experience</label>
         <div class="col-sm-9">
         <input required="required" type="text" value="{{ old('experience') }}" class="form-control form-control-rounded @error('title') is-invalid @enderror" id="input-26" placeholder="Enter Required experience " name="experience">
         </div>
       </div>
 </div>

 <div class="col-md-6">
        <div class="form-group row">
                <label for="input-30" class="col-sm-3 col-form-label">Job Photo</label>
                <div class="col-sm-9">
                <input type="file" class="form-control form-control-rounded @error('logo') is-invalid @enderror" id="input-30" required="required" name="avatar">
                </div>
              </div>
 </div>

 <div class="col-md-6">
    <div class="form-group row">
     <label for="input-26" class="col-sm-3 col-form-label">Apply Link</label>
     <div class="col-sm-9">
     <input  type="text" value="{{ old('link') }}" class="form-control form-control-rounded @error('link') is-invalid @enderror" id="input-26" placeholder="Enter Apply Link" name="link">
     </div>
   </div>
</div>

</div>

        <div class="card-header text-uppercase">Qualification & Instruction</div>
        <div class="card-body">
         <textarea name="body" id="summernoteEditor" style="display: none;" class="@error('body') is-invalid @enderror" required="required"></textarea



      <div class="form-group row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <div class="form-footer">
                <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
            </div>
        </div>
      </div>
      </form>
     </div>
     </div>

        </div>
      </div>

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

	<!--Start footer-->
	<footer class="footer">
      <div class="container">
        <div class="text-center">
          Copyright © 2018 Dashtreme Admin
        </div>
      </div>
    </footer>
    <!--End footer-->

    @include('partials._errors')

  </div><!--End wrapper-->


@endsection
