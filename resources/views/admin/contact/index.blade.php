@extends('layouts.admin')
@section('content')
<div class="clearfix"></div>

  <div class="content-wrapper">
    <div class="container-fluid">
    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
		      <div class="card">
           <div class="card-body">

        <div class="row">

        <!-- Right Sidebar -->
        <div class="col-md-12">

            <div class="card mt-3 shadow-none">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>

                            @forelse ($mails as $mail)
                            <tr>
                                    <td class="mail-rateing">
                                        <i class="fa fa-star"></i>
                                    </td>
                                    <td>
                                        <a href="{{ route('dashboard.mail.show', $mail->id)}}">{{ $mail->name }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('dashboard.mail.show', $mail->id)}}"><i class="fa fa-circle text-info mr-2"></i>{{ $mail->subject }}</a>
                                    </td>
                                    <td>
                                            <a href="mail-read.html"><i class="fa fa-circle text-info mr-2"></i>{{ $mail->email }}</a>
                                        </td>
                                    <td class="text-right">
                                            {{ date("Y M d", strtotime($mail->created_at)) }}
                                    </td>
                                </tr>
                            @empty
                                <p> No emails Yet </p>
                            @endforelse


                            </tbody>
                        </table>
                    </div>

                    <hr>


                </div> <!-- card body -->
            </div> <!-- card -->
        </div> <!-- end Col-9 -->

                    </div><!-- End row -->

             </div>
                    </div>
        </div>
      </div><!-- End row -->

    </div>
    <!-- End container-fluid-->

   </div><!--End content-wrapper-->
@endsection()
