@extends('layouts.admin')

@section('content')

<div class="clearfix"></div>

  <div class="content-wrapper">
    <div class="container-fluid">

    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
		      <div class="card">
           <div class="card-body">

        <div class="row">
        <div class=" col-md-12">
            <div class="row">
                <div class="col-lg-8">
                    <div class="btn-toolbar" role="toolbar">
                        <div class="btn-group mr-1">
                            <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-inbox"></i></button>
                            <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-refresh"></i></button>
                            <button type="button" class="btn btn-light waves-effect waves-light"><i class="fa fa-trash-o"></i></button>
                        </div>
                        <div class="btn-group mr-1">
                            <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-folder"></i>
                            <b class="caret"></b>
                            </button>
                            <div class="dropdown-menu">
                              <a href="javaScript:void();" class="dropdown-item">Action</a>
                              <a href="javaScript:void();" class="dropdown-item">Another action</a>
                              <a href="javaScript:void();" class="dropdown-item">Something else here</a>
                              <div class="dropdown-divider"></div>
                              <a href="javaScript:void();" class="dropdown-item">Separated link</a>
                            </div>
                        </div>
                        <div class="btn-group mr-1">
                            <button type="button" class="btn btn-light waves-effect waves-light dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-tag"></i>
                            <b class="caret"></b>
                            </button>
                            <div class="dropdown-menu">
                              <a href="javaScript:void();" class="dropdown-item">Action</a>
                              <a href="javaScript:void();" class="dropdown-item">Another action</a>
                              <a href="javaScript:void();" class="dropdown-item">Something else here</a>
                              <div class="dropdown-divider"></div>
                              <a href="javaScript:void();" class="dropdown-item">Separated link</a>
                            </div>
                        </div>

                        <div class="btn-group">
                            <button type="button" class="btn btn-light waves-effect waves-light dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              More
                              <span class="caret"></span>
                            </button>
                            <div class="dropdown-menu">
                              <a href="javaScript:void();" class="dropdown-item">Action</a>
                              <a href="javaScript:void();" class="dropdown-item">Another action</a>
                              <a href="javaScript:void();" class="dropdown-item">Something else here</a>
                              <div class="dropdown-divider"></div>
                              <a href="javaScript:void();" class="dropdown-item">Separated link</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="position-relative has-icon-right">
                        <input type="text" class="form-control" placeholder="search mail">
                        <div class="form-control-position">
                            <i class="fa fa-search text-white"></i>
                        </div>
                    </div>
                </div>
            </div> <!-- End row -->
            <div class="card mt-3 shadow-none">
               <div class="card-body">
                 <div class="media mb-3">
                     <div class="media-body">
                        <span class="media-meta float-right">{{ date("Y M d", strtotime($mail->created_at)) }}</span>
                        <h4 class="m-0">{{ $mail->name }}</h4>
                        <small>From : <a href="Milato:{{ $mail->email }}"> {{ $mail->email }} </a></small>
                      </div>
                  </div>

                  <p><b{{ $mail->subject }}</b></p>
                  {{ $mail->message }}
              </div>
            </div> <!-- card -->
        </div> <!-- end Col-9 -->

                    </div><!-- End row -->

             </div>
                    </div>
        </div>
      </div><!-- End row -->

    </div>
    <!-- End container-fluid-->

   </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

@endsection()
