@extends('layouts.admin')
@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Categories</h4>
	   </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Avate</th>
                        <th>Date</th>
                        <th>Control</th>

                    </tr>
                </thead>
                <tbody>

                    @foreach ($categories as $category)

                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->title }}</td>

                        <td>{{ $category->title }}</td>
                        <td>{{ $category->created_at }}</td>
                        <td><div class="btn-group m-1">
                                <form method="post" action="{{ route('dashboard.category.destroy', $category->id ) }}">

                                    @method('DELETE')
                                    @csrf

                                    <button class="delete_btn btn btn-light waves-effect waves-light"> <i class="fa fa fa-trash-o"></i> </button>
                                 </form>

                                <a type="button" href="{{ route('dashboard.category.edit', $category->id)}}" class="btn btn-light waves-effect waves-light"> <i class="fa fa-edit"></i> </a>

                             </div></td>
                    </tr>

                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Avate</th>
                        <th>Date</th>
                        <th>Control</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

  </div><!--End wrapper-->


@endsection
