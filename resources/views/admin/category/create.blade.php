@extends('layouts.admin')
@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('dashboard.category.index')}}">Categories</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add New Category</li>
         </ol>
	   </div>
     </div>

    <!-- End Breadcrumb-->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="personal-info" method="POST" action="{{ route('dashboard.category.store') }}" enctype="multipart/form-data">
                  @csrf
                <h4 class="form-header text-uppercase">

                </h4>
                <div class="form-group row">
                  <label for="input-1" class="col-sm-2 col-form-label">Category Title </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="input-1" name="title" required>
                  </div>
                </div>

                <div class="form-group row">
                    <label for="input-8" class="col-sm-2 col-form-label">Select File</label>
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="input-8" name="avatar" required>
                    </div>
                  </div>
                <div class="form-footer">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div><!--End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

  </div><!--End wrapper-->

  @include('partials._errors')
@endsection
