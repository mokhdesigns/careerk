@extends('layouts.admin')
@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Companies</h4>
	   </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Avatar</th>
                        <th>Location</th>
                        <th>Category</th>
                        <th>Social</th>
                        <th>Control</th>

                    </tr>
                </thead>
                <tbody>

                    @foreach ($companies as $category)

                    <tr>
                        <td>{{ $category->title }}</td>
                        <td>{{ $category->mobile }}</td>
                        <td>{{ $category->email }}</td>
                        <td><img src="{{asset('images/'. $category->avatar) }}" class="company_photo"> </td>
                        <td>{{ $category->location }}</td>
                        <td>{{ $category->category['title'] }}</td>
                        <td style="padding: 10px 0;">
                            <a target="_blank" href="{{ $category->facebook }}" class="btn-social btn-social-circle waves-effect waves-light m-1"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="{{ $category->twitter }}" class="btn-social btn-social-circle waves-effect waves-light m-1"><i class="fa fa-twitter"></i></a>
                            <a target="_blank" href="{{ $category->linkedin }}" class="btn-social btn-social-circle waves-effect waves-light m-1"><i class="fa fa-linkedin"></i></a>

                        </td>
                        <td><div class="btn-group m-1">
                            <form method="post" action="{{ route('dashboard.company.destroy', $category->id ) }}">

                                @method('DELETE')
                                @csrf

                                <button class="delete_btn btn btn-light waves-effect waves-light"> <i class="fa fa fa-trash-o"></i> </button>
                             </form>

                            <a type="button" href="{{ route('dashboard.company.edit', $category->id)}}" class="btn btn-light waves-effect waves-light"> <i class="fa fa-edit"></i> </a>

                         </div></td>
                    </tr>

                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Avatar</th>
                        <th>Location</th>
                        <th>Category</th>
                        <th>Social</th>
                        <th>Control</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->

  </div><!--End wrapper-->


@endsection
