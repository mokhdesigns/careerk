@extends('layouts.admin')
@section('content')

  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('dashboard.company.index')}}">Company</a></li>
            <li class="breadcrumb-item active" aria-current="page">Edit {{ $company->title }}</li>
         </ol>
	   </div>
     </div>

    <!-- End Breadcrumb-->

    <div class="row">
        <div class="col-lg-12">

   <div class="card">
       <div class="card-body">
       <div class="card-title">Edit Company</div>
       <hr>
        <form method="POST" action="{{ route('dashboard.company.update', $company->id ) }}" enctype="multipart/form-data">

            @csrf
            @method('PUT')

       <div class="form-group row">
        <label for="input-26" class="col-sm-2 col-form-label">Company Name</label>
        <div class="col-sm-8">
        <input required="required"  type="text" value="{{ $company->title }}" class="form-control form-control-rounded @error('title') is-invalid @enderror" id="input-26" placeholder="Enter Company Name" name="title">
        </div>
      </div>

      <div class="form-group row">
        <label for="input-27" class="col-sm-2 col-form-label">Company Email</label>
        <div class="col-sm-8">
        <input required="required" value="{{ $company->email }}" type="text" class="form-control form-control-rounded @error('email') is-invalid @enderror" id="input-27" placeholder="Enter Company Email Address" name="email">
        </div>
      </div>

      <div class="form-group row">
        <label for="input-27" class="col-sm-2 col-form-label">Category</label>
        <div class="col-sm-8">
                    <select class="form-control single-select select2-hidden-accessible form-control-rounded" data-select2-id="1" tabindex="-1" aria-hidden="true" name="category_id">
                       @foreach ($categories as $category)
                       <option value="{{ $category->id }} " @if($category->id == $company->category->id) selected @endif>{{ $category->title }}</option>
                       @endforeach

                    </select>
                  </div>
      </div>

      <div class="form-group row">
        <label for="input-28" class="col-sm-2 col-form-label">Company Mobile</label>
        <div class="col-sm-8">
        <input  type="text" value="{{ $company->mobile }}" class="form-control form-control-rounded @error('mobile') is-invalid @enderror" name="mobile" id="input-28" placeholder="Enter Company Mobile Number">
        </div>
      </div>

      <div class="form-group row">
        <label for="input-29" class="col-sm-2 col-form-label">Facebook Link</label>
        <div class="col-sm-8">
        <div class="input-group">
            <div class="input-group-prepend">
              <button class="btn btn-light" type="button"><i class="fa fa-facebook"></i></button>
            </div>
              <input type="text" value="{{ $company->facebook }}" class="form-control @error('facebook') is-invalid @enderror" placeholder="Enter Company Facebook Link" name="facebook">
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label for="input-29" class="col-sm-2 col-form-label">Twitter Link</label>
        <div class="col-sm-8">
        <div class="input-group">
            <div class="input-group-prepend">
              <button class="btn btn-light" type="button"><i class="fa fa-twitter"></i></button>
            </div>
              <input type="text" value="{{ $company->twitter }}" class="form-control @error('twitter') is-invalid @enderror" placeholder="Enter Company Twitter Link" name="twitter">
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label for="input-29" class="col-sm-2 col-form-label">Linkedin Link</label>
        <div class="col-sm-8">
        <div class="input-group">
            <div class="input-group-prepend">
              <button class="btn btn-light" type="button"><i class="fa fa-linkedin"></i></button>
            </div>
              <input type="text" value="{{ $company->linkedin }}" class="form-control @error('linkedin') is-invalid @enderror" placeholder="Enter Company Linkedin Link" name="linkedin">
          </div>
        </div>
      </div>

      <div class="form-group row">
        <label for="input-30" class="col-sm-2 col-form-label">Company Location</label>
        <div class="col-sm-8">
        <input type="text" value="{{ $company->location }}" class="form-control form-control-rounded @error('location') is-invalid @enderror" id="input-30" placeholder="Enter Company Location" name="location" required="required">
        </div>
      </div>

      <div class="form-group row">
        <label for="input-30" class="col-sm-2 col-form-label">Company logo</label>
        <div class="col-sm-8">
        <input type="file" class="form-control form-control-rounded @error('logo') is-invalid @enderror" id="input-30"  name="avatar">
        </div>
      </div>

        <div class="card-header text-uppercase">Company Descreption</div>
        <div class="card-body">
         <textarea name="body" id="summernoteEditor" style="display: none;" class="@error('body') is-invalid @enderror" required="required">{{ $company->body }}</textarea



      <div class="form-group row">
        <label class="col-sm-2 col-form-label"></label>
        <div class="col-sm-10">
            <div class="form-footer">
                <button type="reset" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
            </div>
        </div>
      </div>
      </form>
     </div>
     </div>

        </div>
      </div>

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
    
    @include('partials._errors')

  </div><!--End wrapper-->


@endsection
