@extends('layouts.admin')
@section('content')


  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumb-->
     <div class="row pt-2 pb-2">
        <div class="col-sm-9">
		    <h4 class="page-title">Applied appliess</h4>
	   </div>
     </div>
    <!-- End Breadcrumb-->

      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
              <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Avatar</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Job</th>
                        <th> View Profile </th>
                        <th>Apply Date</th>
                        <th>Controll</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($applies as $applies)

                    <tr>
                        <td>{{ $applies->user->name }}</td>
                        <td><img src="{{asset('images/'. $applies->avatar) }}" class="company_photo"> </td>
                        <td>{{ $applies->user->email}}</td>
                        <td>{{ $applies->user->phone}}</td>
                        <td>{{ $applies->job->title }}</td>
                       <th> <a href="{{ route('candidates.show', $applies->user->id)}}" target="_blank"> <i style="text-align: center;font-size: 42px;display: block;" class="fa fa-eye"></i> </a></th>
                        <td>{{ date("Y M d", strtotime($applies->created_at)) }}</td>
                        <td><div class="btn-group m-1">
                            <form method="post" action="{{ route('dashboard.apply.destroy', $applies->id ) }}">
                                @method('DELETE')
                                @csrf
                                <button class="delete_btn btn btn-light waves-effect waves-light"> <i class="fa fa fa-trash-o"></i> </button>
                             </form>
                         </div></td>

                    </tr>

                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Avatar</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Job</th>
                        <th> View Profile </th>
                        <th>Apply Date</th>
                        <th>Controll</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->

    </div>
    <!-- End container-fluid-->

    </div><!--End content-wrapper-->
   <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->



  </div><!--End wrapper-->



@endsection
