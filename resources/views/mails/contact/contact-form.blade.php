@component('mail::message')
# Introduction

<strong> Name </strong> {{ $data['name']}}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
