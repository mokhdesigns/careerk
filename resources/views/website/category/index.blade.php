@extends('layouts.website')

@section('content')

<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header wform">
							<div class="job-search-sec">
								<div class="job-search">
                                    <h4> Job Categories    </h4>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <div class="block gray double-gap-top">
            <div class="container">
    <div class="cat-sec style2">
        <div class="row">

@foreach ($categories as $category)

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="p-category style2">
                        <a href="{{ route('category.show', $category->id) }}" title="">
                            <i class="la la-graduation-cap"></i>
                            <span>{{ $category->title }}</span>
                        </a>
                        <form method="GET" action="{{ route('job.index')}}">
                        <input class="category_class" type="submit" value="View Category Jobs">
                        <input  type="hidden" value="{{ $category->id }}" name="category">
                        </form>
                    </div>
                </div>
@endforeach
        </div>
        <div class="col-md-12">
                {{$categories->links()}}
</div>

    </div>
            </div>
    </div>
</section>
@endsection()
