@extends('layouts.website')

@section('content')
	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header wform">
							<div class="job-search-sec">
								<div class="job-search">
                                    <h4>Explore Available Jobs </h4>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="job-grid-sec">
							<div class="row">

                                    @foreach ($jobs as $job)
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                        <div class="job-grid">
                                            <div class="job-title-sec">
                                                <div class="c-logo"> <img src="{{ asset('/images/' . $job->avatar ) }}" alt=""> </div>
                                                <h3><a href="#" title="">{{ $job->title }}</a></h3>
                                                <span>{{ $job->company->title }}</span>
                                                {{--  <span class="fav-job"><i class="la la-heart-o"></i></span>  --}}
                                            </div>
                                            <span class="job-lctn">{{ $job->company->location }}</span>
                                            <a href="{{ route('job.show', $job->id) }}" title="">APPLY NOW</a>
                                        </div>
                                    </div>

                                    @endforeach
							</div>
						</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>

@endsection()
