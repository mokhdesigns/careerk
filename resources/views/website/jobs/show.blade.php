@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ asset('/images/resource/mslider1.jpg') }} ) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Careerk Jobs</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
				 	<div class="col-lg-8 column">
				 		<div class="job-single-sec">
				 			<div class="job-single-head2">
				 			 <div class="job-title2">
                                    <img src="{{ asset('images/' . $job->avatar) }}" alt="" style="max-width: 100px;float: left;margin-right: 10px;border-radius: 50%;height: 100px;width: 100px;margin-top: -42px;">
                                <h3>{{ $job->title }}</h3><span class="job-is ft">@if($job->type == 1)  Full Time @elseif($job->type == 2) Part Time  @elseif($job->type == 3) Freelance @elseif($job->type == 4) Internship @endif</span>
				 				<ul class="tags-jobs">
				 					<li><i class="la la-map-marker"></i> {{ $job->company->location }}</li>
				 					<li><i class="la la-money"></i> Type : <span>@if($job->type == 1)  Full Time @elseif($job->type == 2) Part Time  @elseif($job->type == 3) Freelance @elseif($job->type == 4) Internship @endif</span></li>
                                     <li><i class="la la-calendar-o"></i> Post Date: {{ date("Y M d",strtotime($job->created_at)) }}</li>
                                     <li><i class="la la-eye"></i> Views {{ $job->viewed }}</li>
				 				</ul>
				 			</div><!-- Job Head -->
				 			<div class="job-details">
                                 <h3>Job Description</h3>
                                 {!! $job->body !!}
				 			</div>
				 			<div class="job-overview">
					 			<h3>Job Overview</h3>
					 			<ul>
					 				<li><i class="la la-money"></i><h3>Finance</h3><span>@if($job->paid == 1) Paid @else Not Paid @endif</span></li>
					 				<li><i class="la la-thumb-tack"></i><h3>Position</h3><span>{{ $job->position }}</span></li>
					 				<li><i class="la la-puzzle-piece"></i><h3>Category</h3><span>{{ $job->category->title }}</span></li>
					 				<li><i class="la la-shield"></i><h3>Experience</h3><span>{{ $job->experience }}</span></li>
					 				<li><i class="la la-line-chart "></i><h3>Job Type </h3><span>@if($job->type == 1)  Full Time @elseif($job->type == 2) Part Time  @elseif($job->type == 3) Freelance @elseif($job->type == 4) Internship @endif</span></li>
					 			</ul>
					 		</div><!-- Job Overview -->
				 			<div class="share-bar">
				 				<!--<span>Share</span><a href="#" title="" class="share-fb"><i class="fa fa-facebook"></i></a>-->
				 				<!--<a href="#" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>-->
				 			
				 			        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=1808033569436858&autoLogAppEvents=1"></script>

        <div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fcareerk.co%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share Job To Facebook</a></div>
</div>
				 		</div>
                     </div>
				 	</div>
				 	<div class="col-lg-4 column">
				 		<div class="job-single-head style2">
			 				 <img style="min-width: 100px;height: 190px;width: 190px;" src="{{ asset('/images/' . $job->company->avatar) }}" alt="{{ $job->company->title }}" style="max-width: 100px;"/>
			 				<div class="job-head-info">
			 					<h4>{{ $job->company->title }}</h4>
			 					<span>{{ $job->company->location }}</span>
			 					<p><i class="la la-phone"></i> {{ $job->company->mobile }}</p>
                                 <p><i class="la la-envelope-o"></i> {{ $job->company->email }}</p>

                             </div>
@php
            if (Auth::check()){

                   if(is_null($job->link)){
                    @endphp


                        <a href="" title="" class="apply-job-btn" onclick="event.preventDefault();
                        document.getElementById('apply').submit();">
                        <i class="la la-paper-plane"></i>Apply for job</a>

          <form id="apply" action="{{ route('apply.store') }}" method="POST" style="display: none;">
                  @csrf
                  <input type="text" value="{{ $job->id }}" name="job_id">
              </form>

              @php }else{  @endphp
                    <a href="{{ $job->link }}" title="" target="_blank" class="apply-job-btn"><i class="la la-paper-plane"></i>Apply for job</a>

                    @php  }

        }else{  @endphp

                             <a href="{{ route('login') }}" title="" class="apply-job-btn"><i class="la la-paper-plane"></i>Apply for job</a>
@php }
          @endphp
			 				<a href="{{ route('company.show', $job->company_id) }}" title="" class="viewall-jobs">View Company</a>
			 			</div><!-- Job Head -->
				 	</div>
				</div>
			</div>
		</div>
	</section>

@endsection
