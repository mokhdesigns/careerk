
@extends('layouts.website')

@section('content')


	<section>
		<div class="block no-padding">
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="main-featured-sec style2">
							<ul class="main-slider-sec style2 text-arrows">
								<li class="slideHome"><img src="images/resource/mslider3.jpg" alt="" /></li>
								<li class="slideHome"><img src="images/resource/mslider2.jpg" alt="" /></li>
								<li class="slideHome"><img src="images/resource/mslider1.jpg" alt="" /></li>
							</ul>
							<div class="job-search-sec">
								<div class="job-search style2">
									<h3> <img src="{{ asset('images/logo-header.png') }}"> </h3>
									<span>Your Career Path</span>
									<div class="search-job2">

										<form method="get" action="{{ route('job.index') }}">


											<div class="row no-gape">
												<div class="col-lg-4 col-md-3 col-sm-4">
													<div class="job-field">
														<input type="text" placeholder="Keywords" name="search" required/>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4">
													<div class="job-field">
														<select name="type" data-placeholder="All Regions" class="chosen-city">

                                                                 <option name="type" value="1">FUll Time</option>
                                                                 <option name="type" value="2">Part Time</option>
                                                                 <option name="type" value="3">Freelance</option>
                                                                 <option name="type" value="4">Internship</option>

														</select>
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-4">
													<div class="job-field">
														<select name="category_id" data-placeholder="Any category" class="chosen-city">
                                                            @foreach ($categoryDrop as $category)
                                                            <option name="category" value="{{ $category->id }}">{{ $category->title}}</option>
                                                            @endforeach

														</select>
													</div>
												</div>
												<div class="col-lg-2  col-md-3 col-sm-12">
													<button type="submit">FIND JOB <i class="la la-search"></i></button>
												</div>
											</div>
                                        </form>

                                    </div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section>
            <div style="padding-top: 100px;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="heading">
                                <h2 style="margin-bottom: 20px;">About <strong class="main-color"> Careerk </strong></h2>
                                <p style="text-align: left;line-height: 50px;font-size: 18px;">
                                        Nowadays  all the recruitment platforms care about how many companies and how many students they have ,but no one thinks about qualifying youth
                                         to prepare them rightly to the Highly competitive job market
                                         and increase their opportunity to get accepted ,which will reflect directly on the
                                        <span class="main-color">  competence and productivity in our job market </span>
                                </p>
                            </div>
                    </div>
<div class="col-md-5">

<video style="max-width: 100%;" controls>
    <source src="{{ asset('/images/vid.mp4') }}" type="video/mp4">
    <source src="{{ asset('/images/vid.mp4') }}" type="video/ogg">
  Your browser does not support the video tag.
  </video>

</div>
                </div>
            </div>
            </div>
        </section>
        <section>
                <div class="block gray double-gap-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="heading">
                                    <h2>Popular Categories</h2>

                                </div><!-- Heading -->
                                <div class="cat-sec style2">
                                    <div class="row">
@foreach ($categories as $category)

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="p-category style2">
                        <a href="{{ route('category.show', $category->id) }}" title="">
                            <i class="la la-graduation-cap"></i>
                            <span>{{ $category->title }}</span>
                        </a>
                        <form method="GET" action="{{ route('job.index')}}">
                                <input class="category_class" type="submit" value="View Category Jobs">
                                <input  type="hidden" value="{{ $category->id }}" name="category">
                                </form>
                    </div>
                </div>
@endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="browse-all-cat">
                                    <a href="{{ route('category.index')}}" title="" class="style2 noradius">Browse All Categories</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <section>
                <div class="block">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="heading">
                                    <h2>Recent Jobs</h2>

                                </div><!-- Heading -->
                                <div class="job-grid-sec">
                                    <div class="row">

                                @foreach ($jobs as $job)
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                            <div class="job-grid">
                                                <div class="job-title-sec">
                                                    <div class="c-logo"> <img src="{{ asset('/images/' . $job->avatar ) }}" alt=""> </div>
                                                    <h3><a href="{{ route('job.show',$job->id ) }}" title="">{{ $job->title }}</a></h3>
                                                    <span>{{ $job->company->title }}</span>
                                                    {{--  <span class="fav-job"><i class="la la-heart-o"></i></span>  --}}
                                                </div>
                                                <span class="job-lctn">{{ $job->company->location }}</span>
                                                <a href="{{ route('job.show',$job->id ) }}" title="">APPLY NOW</a>
                                            </div>
                                        </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="browse-all-cat">
                                    <a style="background: #2980b9;" href="{{ route('job.index')}}" title="" class="style2">View All Jobs</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


    <section>
            <div class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="heading">
                                <h2>How It Works</h2>
                            </div>
                            <div class="how-to-sec style2">
                                <div class="how-to">
                                    <span class="how-icon"><i class="la la-user"></i></span>
                                    <h3>Register an account </h3>
                                    <p> Sign up today & take your career to the next step</p>
                                </div>
                                <div class="how-to">
                                    <span class="how-icon"><i class="la la-file-archive-o"></i></span>
                                    <h3> Search your job</h3>
                                    <p>  Use our search filter or go to jobs page </p>
                                </div>
                                <div class="how-to">
                                    <span class="how-icon"><i class="la la-list"></i></span>
                                    <h3>Apply for job</h3>
                                    <p> Easy apply for a job and contact us ,if you need help </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section>
            <div class="block gray">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="heading">
                                <h2>Top Company Registered</h2>
                            </div>
                            <div class="top-company-sec">
                                <div class="row" id="companies-carousel">

                                @foreach ($companies as $company)

                                    <div class="col-lg-3">
                                        <div class="top-compnay style2">
                                            <img src="{{ asset('/images/' . $company->avatar ) }}" alt="" />
                                            <h3><a href="{{ route('company.show', $company->id)}}" title="">{{ $company->title }}</a></h3>
                                            <span>{{ $company->location }}</span>
                                            <a href="{{ route('company.show', $company->id)}}" title="">View Company</a>
                                        </div>
                                    </div>

                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>




    <section>
            <div class="block double-gap-top double-gap-bottom">
                <div data-velocity="-.1" style="background: transparent url(&quot;images/resource/parallax1.jpg&quot;) repeat scroll 50% -36.81px;" class="parallax scrolly-invisible layer color"></div><!-- PARALLAX BACKGROUND IMAGE -->
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="simple-text-block">
                                <h3>Make a Difference with Your Career!</h3>
                                <span>Find your dream job in minutes with Careerk!</span>
                                <a href="{{ route('register')}}" title="">Create an Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

	<section>
		<div class="block gray">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2> Careerk Tips</h2>
						</div><!-- Heading -->
						<div class="blog-sec">
							<div class="row">
@foreach ($blogs as $blog)

            <div class="col-lg-4">
                <div class="my-blog">
                    <div class="blog-thumb">
                        <a href="{{ route('blog.show', $blog->id)}}" title="">
                            <img style="max-height: 260px;min-height: 260px;" src="{{ asset('/images/'. $blog->avatar ) }}" alt="{{ $blog->title }}" /></a>
                        <div class="blog-date">
                            <a href="{{ route('blog.show', $blog->id)}}" title="">{{ date("Y", strtotime($blog->created_at)) }} <i>{{ date("M d", strtotime($blog->created_at)) }}</i></a>
                        </div>
                    </div>
                    <div class="blog-details">
                        <h3><a href="{{ route('blog.show', $blog->id)}}" title="">{{ $blog->title }}</a></h3>
                        <p>{!! strip_tags(substr($blog->body, 0, 50)) !!} </p>
                        <a href="{{ route('blog.show', $blog->id)}}" title="">Read More <i class="la la-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>

@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<section>
        <div class="block no-padding"style="margin-top:50px;">
            <div data-velocity="-.1" style="background-image: url( {{ asset('/images/bgcar.jpg') }})" class="parallax scrolly-invisible layer color green2"></div><!-- PARALLAX BACKGROUND IMAGE -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 column">
                        <div class="detailbar">
                            <h3>Are You Hiring?</h3>
                            <p> Find everything you need to post a job and receive the best candidates by visiting our Employer website. We offer small business and enterprise options. Contact us now on : info@careerk.co</p>
                        </div>
                    </div>
                    <div class="col-lg-4 column">
                        <div class="detalbr-mkp" style="margin-top: 0;">
                            <img src="images/resource/mockup4.png" alt="" style="max-width: 354px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection()
