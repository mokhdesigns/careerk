@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Employer</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block less-top">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12 column">
				 		<div class="emply-list-sec">
				 			<div class="row" id="masonry">
@foreach ($companies as $company)


            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
                <div class="emply-list box">
                    <div class="emply-list-thumb">
                        <a href="{{ route('company.show', $company->id ) }}" title="">
                            <img src="{{ asset('/images/' . $company->avatar) }}" alt="{{ $company->title }}" style="max-height: 100px;" /></a>
                    </div>
                    <div class="emply-list-info">
                        <h3><a href="{{ route('company.show', $company->id ) }}" title="">{{ $company->title }}</a></h3>
                        <h6><i class="la la-map-marker"></i> {{ $company->location }}</h6>
                    </div>
                </div><!-- Employe List -->
                </div>
                                 @endforeach
							</div>
				 		</div>
					</div>
					<div class="col-md-12">
							{{$companies->links()}}
</div>
		
				 </div>
			</div>
		</div>
	</section>


@endsection()
