
@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ asset('/images/resource/mslider1.jpg') }}) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{ $company->title }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
				 	<div class="col-lg-12 column">
				 		<div class="job-single-sec style3">
				 			<div class="job-head-wide">
				 				<div class="row">
				 					<div class="col-lg-10">
				 						<div class="job-single-head3 emplye">
							 				<div class="job-thumb"> <img src="{{ asset('images/' . $company->avatar) }}" alt="{{ $company->title }}" /></div>
							 				<div class="job-single-info3">
							 					<h3>{{ $company->title }}</h3>
							 					<span><i class="la la-map-marker"></i>{{ $company->location }}</span>
                                                 <div class="share-bar">
                                                        <a href="{{ $company->facebook }}" target="_blank" title="" class="share-fb"><i class="fa fa-facebook"></i></a>
                                                        <a href="{{ $company->twitter }}" target="_blank" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>
                                                        <a href="{{ $company->linkedin }}" target="_blank" title="" class="share-twitter"><i class="fa fa-linkedin"></i></a>
                                                    </div>
							 				</div>
							 			</div>
				 					</div>
				 				</div>
				 			</div>
				 			<div class="job-wide-devider">
							 	<div class="row">
							 		<div class="col-lg-8 column">
							 			<div class="job-details">
                                             <h3>About {{ $company->title }}</h3>

							 				{!! $company->body !!}
							 			</div>
								 		<div class="recent-jobs">
							 				<h3>Jobs from {{ $company->title }}</h3>
							 				<div class="job-list-modern">
											 	<div class="job-listings-sec no-border">

                        @forelse ($jobs as $job)

                                <div class="job-listing wtabs noimg">

                                    <div class="job-title-sec">
                                            <a href="{{ route('job.show', $job->id)}}">
                                        <h3><a href="{{ route('job.show', $job->id)}}" title="">{{ $job->title }}</a></h3>
                                        <span>Massimo Artemisis</span>
                                        <div class="job-lctn"><i class="la la-map-marker"></i>{{ $job->company->location }}</div>
                                            </a>
                                    </div>
                                    <div class="job-style-bx">
                                            <a href="{{ route('job.show', $job->id)}}">
                                        <span class="job-is ft">@if($job->type == 1)  Full Time @elseif($job->type == 2) Part Time  @elseif($job->type == 3) Freelance @endif</span>
                                        <span class="fav-job"><i class="la la-heart-o"></i></span>
                                        <i>{{ date("Y M d", strtotime($job->created_at)) }}</i>
                                    </a>
                                    </div>

                                </div>
                                @empty
                                <h5 class="text-center text-danger"> No Jobs Yet </h5>
                                @endforelse

												</div>
											 </div>
							 			</div>
							 		</div>
							 		<div class="col-lg-4 column">
							 			<div class="job-overview">
								 			<h3>Company Information</h3>
								 			<ul>
								 				<li><i class="la la-file-text"></i><h3>Posted Jobs</h3><span>{{$jobsCount }}</span></li>
								 				<li><i class="la la-map"></i><h3>Locations</h3><span>{{ $company->location}}</span></li>
								 				<li><i class="la la-bars"></i><h3>Categories</h3><span>{{ $company->category['title']}}</span></li>
								 				<li><i class="la la-clock-o"></i><h3>Since</h3><span>{{ date("Y M d", strtotime($company->created_at) )}}</span></li>
								 				<li><i class="la la-phone"></i><h3>Phone</h3><span>{{ $company->mobile}}</span></li>
								 				<li><i class="la la-envelope"></i><h3>Email</h3><span>{{ $company->email}}</span></li>
								 			</ul>
								 		</div>
							 		</div>
							 	</div>
							 </div>
					 	</div>
				 	</div>
				</div>
			</div>
		</div>
	</section>

@endsection()
