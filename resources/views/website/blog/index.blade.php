@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Careerk Tips</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="blog-sec">
							<div class="row" id="masonry">
@foreach ($blogs as $blog)


            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <div class="my-blog">
                    <div class="blog-thumb">
                        <a href="{{ route('blog.show', $blog->id)}}" title="">
                            <img style="max-height: 260px;min-height: 260px;" src="{{ asset('/images/' . $blog->avatar) }}" alt="{{ $blog->title }}" /></a>
                        <div class="blog-metas">
                                <a href="{{ route('blog.show', $blog->id)}}" title="{{ $blog->title }}">{{ date("Y", strtotime($blog->created_at)) }} <i>{{ date("M d", strtotime($blog->created_at)) }}</i></a>

                        </div>
                    </div>
                    <div class="blog-details">
                        <h3><a href="#" title="{{ $blog->title }}">{{ $blog->title }}</a></h3>
                        <p>{!! strip_tags(substr($blog->body, 0, 50)) !!} </p>
                        <a href="{{ route('blog.show', $blog->id)}}" title="">Read More <i class="la la-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>

            @endforeach
							</div>

						</div>
					</div>
					<div class="col-md-12">
							{{$blogs->links()}}
</div>
				 </div>
			</div>
		</div>
	</section>


</div>

@endsection
