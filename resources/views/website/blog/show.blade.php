@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ asset('images/resource/mslider1.jpg')}}" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>{{ $blog->title}}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">

				 <div class="row">
				 	<div class="col-lg-9 column">
				 		<div class="blog-single">
				 			<div class="bs-thumb">
                             <img  src="{{ asset('/images/' . $blog->avatar) }}" alt="{{ $blog->title }}" />
                             </div>

				 		 {!! $blog->body !!}
				 			<div class="tags-share">
						 		<div class="share-bar">
					 				<a href="#" title="" class="share-fb"><i class="fa fa-facebook"></i></a><a href="#" title="" class="share-twitter"><i class="fa fa-twitter"></i></a><a href="#" title="" class="share-google"><i class="la la-google"></i></a><span>Share</span>
					 			</div>
				 			</div>
				 			<div class="post-navigation ">
				 				<div class="post-hist prev">
				 					<a href="{{ route('blog.show', $blog->id-1) }}" title=""><i class="la la-arrow-left"></i><span class="post-histext">Prev Post<i></i></span></a>
				 				</div>
				 				<div class="post-hist next">
				 					<a href="{{ route('blog.show', $blog->id+1) }}" title=""><span class="post-histext">Next Post<i></i></span><i class="la la-arrow-right"></i></a>
				 				</div>
                             </div>


				 			<div class="comment-sec">
                                 <h3> Comments</h3>
                                 @forelse ($comments as $comment)
                                 <div class="job-listing wtabs" style="margin-top: 5px;padding: 5px 10px;">
                                        <div class="job-title-sec">
                                            <div class="c-logo" style="width: 80px;height: 80px;margin-right: 9px;"> <img style="min-width: 100%;min-height: 100%;border-radius: 50%;" src="{{ asset('images/' . $comment->user->avatar )}}" alt=""> </div>
                                            <h3><a href="#" title="">{{ $comment->user->name }}</a></h3>
                                            <span>{{ $comment->user->career}}</span>
                                            <div class="job-lctn">{{ $comment->body}}</div>
                                            <p style="margin-left: 10px;font-size: 0px;"> {{ date("Y M d", strtotime($comment->created_at)) }} </p>
                                        </div>
                                    </div>

                        @empty
                            <h6 class="text-center text-danger"> No Comments Yet </h6>
                        @endforelse

				 			
                             </div>




				 			<div class="commentform-sec">
                                    @if (Auth::check())
				 				<h3>Leave a Reply</h3>
				 				<form action="{{ route('comment.store')}}" method="POST">
                                     @csrf
				 					<div class="row">
				 						<div class="col-lg-12">
					 						<span class="pf-title">Description</span>
					 						<div class="pf-field">
                                                 <textarea name="body"></textarea>
                                                 <input type="hidden" name="blog_id" value="{{ $blog->id}}"
					 						</div>
					 					</div>
					 					<div class="col-lg-12">
					 						<button type="submit">Post Comment</button>
					 					</div>
				 					</div>
                                 </form>
                                 @else
                                 <h3>Please Login To Leave A reply</h3>
                                 @endif
				 			</div>
				 		</div>
                     </div>
					</div>
					<aside class="col-lg-3 column">
                            <div class="widget">
                                    <h3>Recent Posts</h3>
                                    <div class="post_widget">
       @foreach ($recents as $recent)

                                        <div class="mini-blog">
                                            <span><a href="{{ route('blog.show', $recent->id) }}" title=""><img src="{{ asset('images/' . $recent->avatar ) }}" alt="{{ $recent->title }}" /></a></span>
                                            <div class="mb-info">
                                                <h3><a href="{{ route('blog.show', $recent->id) }}" title="">{{ $recent->title }}</a></h3>
                                                {{-- <span>{{ date("M  d Y", strtotime($recent->created_at)) }} </span> --}}
                                            </div>
                                        </div>

                                        @endforeach
                                    </div>
                         </div>
					</aside>
				 </div>
			</div>
		</div>
	</section>
@endsection
