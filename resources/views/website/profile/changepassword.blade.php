@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Change Your Password</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">


                        <aside class="col-lg-3 column border-right">
                                <div class="widget">
                                    <div class="tree_widget-sec">
                                        <ul>
                                            <li><a href="{{ route('profile.index') }}" title=""><i class="la la-file-text"></i>My Profile</a></li>
                                           <li><a href="{{ asset('images/' . Auth::user()->cv) }}" target="_blank" title=""><i class="la la-briefcase"></i>My Resume</a></li>
                                           <li><a class="view-resume-list" title=""><i class="la la-paper-plane"></i>Applied Job</a></li>
                                           <li><a href="{{ route("profile.edit" , Auth::user()->id)}}" title=""><i class="la la-file-text"></i> Edit Profile</a></li>
                                           <li><a href="{{ route('profile.show', Auth::user()->id )}}" title=""><i class="la la-flash"></i>Change Password</a></li>
                                           <li class="signup-popup">
                                                <a title=""href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                              document.getElementById('logout-form').submit();"><i class="la la-unlink"></i>Sign Out</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="widget">
                                    <div class="skill-perc">
                                        <h3>Profile Progress </h3>
                                        <div class="skills-bar">
                                            <span>85%</span>
                                            <div
                                                class="second circle"
                                                data-size="155"
                                                data-thickness="60">
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>


				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">
					 			<h3>Change Password</h3>
						 		<div class="change-password">
						 			<form method="post" action="{{ route('profile.store') }}">
                                         @csrf
						 				<div class="row">
						 					<div class="col-lg-6">
						 						<span class="pf-title">Old Password</span>
						 						<div class="pf-field">
						 							<input type="password" name="old_password" required />
						 						</div>
						 						<span class="pf-title">New Password</span>
						 						<div class="pf-field">
						 							<input type="password" name="password" required/>
						 						</div>
						 						<span class="pf-title">Confirm Password</span>
						 						<div class="pf-field">
						 							<input type="password" name="password_confirmation" />
						 						</div>
						 						<button type="submit">Update</button>
						 					</div>
						 					<div class="col-lg-6">
						 						<i class="la la-key big-icon"></i>
						 					</div>
						 				</div>
						 			</form>
						 		</div>
					 		</div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>
</div>


@include('partials._errors')
@endsection()
