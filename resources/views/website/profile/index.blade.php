@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ asset('images/resource/mslider1.jpg') }} repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Welcome {{ Auth::user()->name }}</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">

				 	<aside class="col-lg-3 column border-right">
				 		<div class="widget">
				 			<div class="tree_widget-sec">
				 				<ul>
				 					<li><a href="{{ route('profile.index') }}" title=""><i class="la la-file-text"></i>My Profile</a></li>
									<li><a href="{{ asset('images/' . Auth::user()->cv) }}" target="_blank" title=""><i class="la la-briefcase"></i>My Resume</a></li>
									<li><a class="view-resume-list" title=""><i class="la la-paper-plane"></i>Applied Job</a></li>
									<li><a href="{{ route("profile.edit" , Auth::user()->id)}}" title=""><i class="la la-file-text"></i> Edit Profile</a></li>
									<li><a href="{{ route('profile.show', Auth::user()->id )}}" title=""><i class="la la-flash"></i>Change Password</a></li>
                                    <li class="signup-popup">
                                            <a title=""href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();"><i class="la la-unlink"></i>Sign Out</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                        </li>
				 				</ul>
				 			</div>
				 		</div>
				 		<div class="widget">
				 			<div class="skill-perc">
				 				<h3>Profile Progress </h3>
				 				<div class="skills-bar">
				 					<span>85%</span>
				 					<div
				 						class="second circle"
				 						data-size="155"
				 						data-thickness="60">
								    </div>
				 				</div>
				 			</div>
				 		</div>
                     </aside>


                     <div class="col-lg-9 column">
				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">

                                    <div class="edu-history-sec" style="padding-top: 28px;">
                                            <div class="edu-history">
                                               <img style="width: 100px;height: 100px;border-radius: 50%;border: 4px solid #8b91dd;" src="{{ asset('images/' . Auth::user()->avatar) }}"
                                                <div class="edu-hisinfo">
                                                    <div class="student-info" style="display: inline-block;">
                                                    <h3 style="margin-top: 10px;text-transform: capitalize;">{{ Auth::user()->name }}</h3>
                                                    <h6>{{ Auth::user()->email }}</h6>
                                                    <h6>{{ Auth::user()->phone }}</h6>
                                                    <h6>{{ Auth::user()->career }}</h6>

                                                    <p style="color:#8b91dd"> Contact Information Are @if(Auth::user()->public == 1 )
                                                        Public
                                                        @else
                                                        Private
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

						 		<div class="cat-sec">
									<div class="row no-gape">

										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="p-category view-resume-list">
												<a href="#" title="">
													<i class="la la-briefcase"></i>
													<span>Applied Job</span>
													<p> {{ $appliedCount }} Applications</p>
												</a>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-12">
											<div class="p-category">
												<a href="{{ asset('images/' . Auth::user()->cv) }}" target="_blank" title="">
													<i class="la la-file-text "></i>
													<span>My CV</span>
													<p>View My CV</p>
												</a>
											</div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-12">
											<div class="p-category follow-companies-popup">
												<a href="#" title="">
													<i class="la la-envelope"></i>
													<span>Messages</span>
													<p>0 New Messages </p>
												</a>
											</div>
										</div>
									</div>
                                </div>

								<div class="cat-sec">


				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">

		 						<div class="border-title"><h3>Professional Skills</h3></div>
		 						<div class="edu-history-sec">
		 							<div class="edu-history style2">
		 								<i></i>
		 								<div class="edu-hisinfo">
		 									<h3>About</h3>
                                             <p>{{ Auth::user()->body }}</p>
		 								</div>
		 							</div>
		 							<div class="edu-history style2">
		 								<i></i>
		 								<div class="edu-hisinfo">
		 									<h3> 	Experience </h3>
                                             <p>{{ Auth::user()->experience  }}</p>
		 								</div>
		 							</div>
		 							<div class="edu-history style2">
		 								<i></i>
		 								<div class="edu-hisinfo">
                                             <h3>Education</h3>

                                             <p>{{ Auth::user()->education  }}</p>
		 								</div>

		 							</div>
		 						</div>
					 		</div>
                         </div>

					</div>
				 </div>
			</div>
		</div>
	</section>



								</div>
					 		</div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>

</div>


<div class="view-resumesec">
	<div class="view-resumes">
		<span class="close-resume-popup"><i class="la la-close"></i></span>
        <h3>You applied for this jobs .</h3>

        @forelse ($applied as $item)

		<div class="job-listing wtabs" style="width: 50%;">
			<div class="job-title-sec">
				<div class="c-logo"> <img src="{{ asset('images/' . $item->avatar) }}" alt="" style="width: 100px;height: 100px;max-height: 100px;border-radius: 50%;min-height: 100px;margin-top: -21px;" /> </div>
				<h3><a href="{{ route('job.show', $item->id) }}" title="">{{ $item->title }}</a></h3>
				<span>{{ $item->position }}</span>
			</div>
        </div>
        @empty
           <h5> You Didnt Apply For Any Jobs </h5>
        @endforelse


	</div>
</div>

<div class="follow-companiesec">
	<div class="follow-companies">
		<span class="close-follow-company"><i class="la la-close"></i></span>
		<h3>Messages .</h3>
		<ul id="scrollbar">

	<h4 class="text-danger text-center"> You Have No Messages </h4>

		</ul>
	</div>
</div>



@endsection()
