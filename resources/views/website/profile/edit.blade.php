@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Edit My Profile </h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">

                        <aside class="col-lg-3 column border-right">
                                <div class="widget">
                                    <div class="tree_widget-sec">
                                        <ul>
                                            <li><a href="{{ route('profile.index') }}" title=""><i class="la la-file-text"></i>My Profile</a></li>
                                           <li><a href="{{ asset('images/' . Auth::user()->cv) }}" target="_blank" title=""><i class="la la-briefcase"></i>My Resume</a></li>
                                           <li><a class="view-resume-list" title=""><i class="la la-paper-plane"></i>Applied Job</a></li>
                                           <li><a href="{{ route("profile.edit" , Auth::user()->id)}}" title=""><i class="la la-file-text"></i> Edit Profile</a></li>
                                           <li><a href="candidates_change_password.html" title=""><i class="la la-flash"></i>Change Password</a></li>
                                           <li class="signup-popup">
                                                <a title=""href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                              document.getElementById('logout-form').submit();"><i class="la la-unlink"></i>Sign Out</a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="widget">
                                    <div class="skill-perc">
                                        <h3>Profile Progress </h3>
                                        <div class="skills-bar">
                                            <span>85%</span>
                                            <div
                                                class="second circle"
                                                data-size="155"
                                                data-thickness="60">
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </aside>

				 	<div class="col-lg-9 column">
				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">

						 		<div class="coverletter-sec">
						 			<form method="POST" action="{{ route('profile.update', $user->id) }}" enctype="multipart/form-data">

                                        @csrf
                                        @method('PUT')
						 				<div class="row">
						 					<div class="col-lg-6">
						 						<span class="pf-title">Your Name </span>
						 						<div class="pf-field">
						 							<input type="text" value="{{ $user->name }}" required name="name">
						 						</div>
                                             </div>

						 					<div class="col-lg-6">
                                                <span class="pf-title">Your Phone </span>
                                                <div class="pf-field">
                                                    <input type="text" value="{{ $user->phone }}" required name="phone">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Email </span>
                                                <div class="pf-field">
                                                    <input type="text" value="{{ $user->email }}" required name="email">
                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Career </span>
                                                <div class="pf-field">
                                                    <input type="text" value="{{ $user->career }}" required name="career">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Cv </span>
                                                <div class="pf-field">
                                                    <input type="file" value="{{ $user->cv }}"  name="cv">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Photo </span>
                                                <div class="pf-field">
                                                    <input type="file" value="{{ $user->avatar }}"  name="avatar">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Gender</span>
                                                <div class="pf-field">
                                                    <select data-placeholder="Select Your CV" class="chosen" name="gender">
                                                       <option @if($user->gender == 0) 'selected' @endif value="0">Male</option>
                                                       <option  @if($user->gender == 1) 'selected' @endif value="1">Female</option>
                                                   </select>
                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                    <span class="pf-title">View Contact Information To Public</span>
                                                    <div class="pf-field">
                                                        <select  class="chosen" name="public">
                                                           <option  @if($user->public == 1) selected="selected" @endif value="1">Show</option>
                                                           <option   @if($user->public == 0) selected="selected" @endif value="0">Hide</option>
                                                       </select>
                                                    </div>
                                                </div>



                                            <div class="col-lg-12">
                                                <span class="pf-title">About You</span>
                                                <div class="pf-field">
                                                    <textarea name="body"> {{ $user->body }}</textarea>
                                                </div>
                                            </div>

						 					<div class="col-lg-12">
						 						<span class="pf-title">Your Education</span>
						 						<div class="pf-field">
						 							<textarea name="education"> {{ $user->education }}</textarea>
						 						</div>
                                             </div>

                                             <div class="col-lg-12">
                                                <span class="pf-title">Your experience</span>
                                                <div class="pf-field">
                                                    <textarea name="experience"> {{ $user->experience }}</textarea>
                                                </div>
                                            </div>



						 					<div class="col-lg-12">
						 						<button type="submit">Update</button>
						 					</div>
						 				</div>
						 			</form>
						 		</div>
					 		</div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>
    @include('partials._errors')
</div>


@endsection()
