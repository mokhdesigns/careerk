@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url({{ asset('images/resource/mslider1.jpg') }} ) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div>
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="overlape">
		<div class="block remove-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="cand-single-user">
							<div class="share-bar circle">
                                 <a href="#" target="_blank" title="" class="share-google"><i class="la la-linkedin"></i></a>
                                 <a href="#" target="_blank" title="" class="share-fb"><i class="fa fa-facebook"></i></a>
                                 <a href="#" target="_blank" title="" class="share-twitter"><i class="fa fa-twitter"></i></a>
				 			</div>
				 			<div class="can-detail-s">
				 				<div class="cst"><img src="{{ asset('images/default.png') }}" alt="" /></div>
				 				<h3>{{ $user->name }}</h3>
				 			</div>
				 			<div class="download-cv">
				 				<a href="{{ $user->cv }}" target="_blank" title="">Download CV <i class="la la-download"></i></a>
				 			</div>
				 		</div>
				 		<div class="cand-details-sec">
				 			<div class="row">
				 				<div class="col-lg-8 column">
				 					<div class="cand-details" id="about">
                                         <h2> About {{ $user->name }} </h2>

                                         {!! $user->body !!}

				 						<div class="edu-history-sec" id="education">
				 							<h2>Education</h2>
				 							<div class="edu-history">
				 								<i class="la la-graduation-cap"></i>
				 								<div class="edu-hisinfo">
                                                        {!! $user->education !!}

				 								</div>
				 							</div>
				 						</div>
				 						<div class="edu-history-sec" id="experience">
				 							<h2>Experience</h2>
				 							<div class="edu-history style2">
				 								<i></i>
				 								<div class="edu-hisinfo">
                                                        {!! $user->experience !!}
				 								</div>
				 							</div>
				 						</div>
				 					</div>
				 				</div>
				 				<div class="col-lg-4 column">
						 			<div class="job-overview">
							 			<h3>General Information</h3>
							 			<ul>
							 				<li><i class="la la-phone"></i><h3>Phone</h3><span>{{ $user->phone }}</span></li>
							 				<li><i class="la la-mars-double"></i><h3>Gender</h3><span>{{ $user->gender }}</span></li>
							 				<li><i class="la la-envelope"></i><h3>Email</h3><span>{{ $user->email }}</span></li>
                                         </ul>
                                         <div class="download-cv">
                                                <a href="" target="_blank" title="">Edit Profile<i class="la la-download"></i></a>
                                            </div>
							 		</div>
						 		</div>
				 			</div>
				 		</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection()
