
@extends('layouts.website')

@section('content')

<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header wform">
							<div class="job-search-sec">
								<div class="job-search">
                                    <h4> What Is Careerk </h4>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section>
		<div class="block">
			<div class="container">

				 <div class="row">
				 	<div class="col-lg-12">
				 		<div class="about-us">
				 			<div class="row">
				 				<div class="col-lg-12">
				 					<h3> Rejected !!</h3>
				 				</div>
				 				<div class="col-lg-7">

				 					<p style="font-size: 14px;line-height: 30px;margin-top: 18px;">


                                We all did ,It’s just the first step in your career path. </br>

                                Careerk ,The First Career Community will help you either :</br>
                                <ul>
                              <li style="list-style-type: square;">  You don’t Know what career fits you </li>
                              <li style="list-style-type: square;">  How you should develop yourself to meet the job market criteria </li>
                              <li style="list-style-type: square;">  Or finding the opportunities that would help you to advance your career </li>

                               <h5 class="color"> Join Our Community Today! </h5>
                                    and take the first step in your career.

                                     </p>
				 				</div>
				 				<div class="col-lg-5">
				 					<img src="images/resource/bsd1.jpg" alt="" />
				 				</div>
                             </div>

                             </div>

				 		</div>
				 	</div>
				 </div>
			</div>
		</div>
	</section>

	<section style="padding: 50px 0; color:#fff">

			<div data-velocity="-.1" style="background: url(images/mediabg.png) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible layer color light"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container">
                <h3 class="text-center" style="margin-bottom: 50px;"> Achievements</h3>
				<div class="row">

                        <div class="col-md-6">
                                <h5> OFFLINE ACTIVITIES </h5>
                               <p style="color:#fff"> We have helped more than 1000 student all over Egypt with our offline activities ;
                                   camps ,seminars ,workshops ,Job fairs and Mega Events  ... </p>
                                   <p style="color:#fff"> We have been in : </p>
                                   <ul class="about-achievments">
                                          <li> Helwan University </li>
                                          <li> Ain Shams University</li>
                                          <li> Assiut University</li>
                                          <li> Sohag University </li>
                                          <li> IAEMS University </li>
                                          <li> Misr Public Library </li>
                                          <li> Applied Arts Tower</li>
                                   </ul>
         </div>
         <div class="col-md-6">
                                   <h5> MEDIA COVERAGE</h5>
                                   <h6> <a href="http://bit.ly/2J505BJ" target="_blank">AL-Tab3a Al-Oula </a></h6>
                                   <h6> <a href="http://bit.ly/2VcmbIN" target="_blank"> Al-Ra2y Al3am </a></h6>
                                   <div class="col-lg-5">
                                        <img src="images/resource/media.jpg" alt="" />
                                    </div>

                            </div>

                </div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="stats-sec style2">
							<div class="row">
								<div class="col-lg-3  col-md-3 col-sm-6 col-xs-6">
									<div class="stats">
										<span>{{ $jobCount}}</span>
										<h5>Jobs Posted</h5>
									</div>
								</div>
								<div class="col-lg-3  col-md-3 col-sm-6 col-xs-6">
									<div class="stats">
										<span>{{ $studentCount}}</span>
										<h5>Users</h5>
									</div>
								</div>
								<div class="col-lg-3  col-md-3 col-sm-6 col-xs-6">
									<div class="stats">
										<span>{{ $companyCount}}</span>
										<h5>Companies</h5>
									</div>
								</div>
								<div class="col-lg-3  col-md-3 col-sm-6 col-xs-6">
									<div class="stats">
										<span>{{ $categoryCount}}</span>
										<h5>Categories</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block">
			<div data-velocity="-.1" style="background: url(images/resource/parallax3.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="heading">
							<h2> Careerk Tips</h2>

						</div><!-- Heading -->
						<div class="blog-sec">
							<div class="row">

                                    @foreach ($blogs as $blog)

                                    <div class="col-lg-4">
                                        <div class="my-blog">
                                            <div class="blog-thumb">
                                                <a href="{{ route('blog.show', $blog->id)}}" title="">
                                                    <img style="height: 300px;" src="{{ asset('/images/'. $blog->avatar ) }}" alt="{{ $blog->title }}" /></a>
                                                <div class="blog-date">
                                                    <a href="{{ route('blog.show', $blog->id)}}" title="">{{ date("Y", strtotime($blog->created_at)) }} <i>{{ date("M d", strtotime($blog->created_at)) }}</i></a>
                                                </div>
                                            </div>
                                            <div class="blog-details">
                                                <h3><a href="{{ route('blog.show', $blog->id)}}" title="">{{ $blog->title }}</a></h3>
                                                <p>{!! strip_tags(substr($blog->body, 0, 50)) !!} </p>
                                                <a href="{{ route('blog.show', $blog->id)}}" title="">Read More <i class="la la-long-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>

                        @endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    @endsection()
