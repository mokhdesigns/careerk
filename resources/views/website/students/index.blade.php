@extends('layouts.website')

@section('content')

	<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header">
							<h3>Our Successfull Candidates </h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="block remove-bottom">
			<div class="container">
				 <div class="row">


                                @foreach ($students as $student)
                             <div class="col-md-6">
				 			<div class="emply-resume-list square">
				 				<div class="emply-resume-thumb">
				 					<img src="{{ asset('images/' . $student->avatar) }}" alt="{{ $student->name }}" />
				 				</div>
				 				<div class="emply-resume-info">
				 					<h3><a href="{{ route('candidates.show', $student->id ) }}" title="">{{ $student->name }}</a></h3>
				 					<span> {{ $student->career }}</span>
				 					<p><i class="la la-map-marker"></i>{{ $student->location }}</p>
				 				</div>
				 				<div class="shortlists">
				 					<a href="{{ route('candidates.show', $student->id ) }}" title="">View Profile <i class="la la-eye"></i></a>
				 				</div>
                             </div><!-- Emply List -->
                             </div>

                             @endforeach
                        </div>
					</div>
				 </div>
			</div>
		</div>
	</section>

@endsection()
