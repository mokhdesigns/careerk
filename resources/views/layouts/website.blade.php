<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{{ config('app.name', 'careerk') }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="CreativeLayers">

	<!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('website/css/bootstrap-grid.css') }}" />
    <link rel="icon" href="{{ asset('/images/logo-icon.png') }}" type="image/x-icon">
	<link rel="stylesheet" href="{{ asset('website/css/icons.css') }}">
	<link rel="stylesheet" href="{{ asset('website/css/animate.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('website/css/style.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('website/css/responsive.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('website/css/chosen.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('website/css/colors/colors.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('website/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('website/css/font-awesome.min.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface&display=swap" rel="stylesheet">
    <script src="{{ asset('website/js/jquery.min.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('admin/plugins/notifications/css/lobibox.min.css') }}"/>

</head>
<body>

<div class="page-loading">
<div class='peeek-loading'>
  <ul>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
</div>


</div>
<div class="theme-layout" id="scrollup">

		<div class="responsive-header">
			<div class="responsive-menubar">
				<div class="res-logo"><a href="" title=""><img src="{{ asset('images/Careerk-logo.png')}}" alt="" /></a></div>
				<div class="menu-resaction">
					<div class="res-openmenu">
						<img src="images/icon.png" alt="" /> Menu
					</div>
					<div class="res-closemenu">
						<img src="{{ asset('images/Careerk-logo.png')}}" alt="" /> Close
					</div>
				</div>
			</div>
			<div class="responsive-opensec">
				<div class="btn-extars">
					<ul class="account-btns">
						<li class="signup-popup"><a title="" href="{{ route('register') }}"><i class="la la-key"></i> Sign Up</a></li>
						<li class="signin-popup"><a title="" href="{{ route('login') }}"><i class="la la-external-link-square"></i> Login</a></li>
					</ul>
				</div><!-- Btn Extras -->

				<div class="responsivemenu">
					<ul>
                        <li> <a href="{{ url('/') }}" title="">Home</a></li>
                        <li> <a href="{{ url('about') }}" title="">About us</a></li>
                        <li> <a href="{{ route('company.index') }}" title="">Companies</a></li>
                        <!--<li> <a href="{{ route('candidates.index') }}" title="">Candidates</a></li>-->
                        <li> <a href="{{ route('job.index') }}" title="">Jobs</a></li>
                        <li> <a href="{{ route('blog.index') }}" title="">Careerk Tips</a></li>
                        <li> <a href="{{ route('contact.index') }}" title="">Contact</a></li>
						</ul>
				</div>
			</div>
		</div>

		<header class="stick-top style2">
			<div class="menu-sec">
				<div class="container fluid">
					<div class="logo">
						<a href="" title=""><img src="{{ asset('images/Careerk-logo.png')}}" style="max-width: 226px;margin-top: -43px;" alt="" /></a>
					</div><!-- Logo -->
					<div class="btn-extars">
						<ul class="account-btns">

                                @if (Auth::check() && Auth::user()->role_id == 2)

                                <div class="my-profiles-sec">
                                        <span><img style="width: 50px;height: 50px;" src="{{ asset('images/' . Auth::user()->avatar ) }}" alt=""> {{ Auth::user()->name}} <i class="la la-bars"></i></span>
                                    </div>
                            @elseif (Auth::check() && Auth::user()->role_id == 1)
                            <li class=""><a href="{{ route('dashboard.') }}" title=""><i class="la la-user"></i> Dashboard </a></li>

                                @else
                                <li class="signup-popup"><a href="{{ route('register') }}" title=""><i class="la la-key"></i> Sign Up</a></li>
                                <li class="signin-popup"><a href="{{ route('login') }}" title=""><i class="la la-external-link-square"></i> Login</a></li>
                                @endif

						</ul>
                    </div><!-- Btn Extras -->
					<nav>
						<ul>
							<li> <a href="{{ url('/') }}" title="">Home</a></li>
                            <li> <a href="{{ url('about') }}" title="">About us</a></li>
                            <li> <a href="{{ route('company.index') }}" title="">Companies</a></li>
                            <!--<li> <a href="{{ route('candidates.index') }}" title="">Candidates</a></li>-->
                            <li> <a href="{{ route('job.index') }}" title="">Jobs</a></li>
                            <li> <a href="{{ route('blog.index') }}" title="">Careerk Tips</a></li>
                            <li> <a href="{{ url('contact') }}" title="">Contact</a></li>
						</ul>
					</nav><!-- Menus -->
				</div>
			</div>
		</header>

@yield('content')
<footer>
    <div class="bottom-line">
        <div class="container">
        <div class="row">
            <div class="col-lg-3 column">
                    <div class="widget">
                        <div class="about_widget">
                            <div class="logo">
                                <a href="" title=""><img src="{{ asset('/images/Careerk-logo-white.png') }}" style="max-width: 179px;margin-bottom: -32px;" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 column">
                        <div class="widget">
                            <div class="about_widget">
                                <span>010640355665</span>
                                <span>info@careerk.co</span>
                                <span> support@careerk.co  </span>
                            </div>
                        </div>
                    </div>
                <div class="col-md-3">
                        <div class="social">
                                <a href="https://www.facebook.com/carrerk" target="_blank" title=""><i class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/Careerk2" target="_blank" title=""><i class="fa fa-twitter"></i></a>
                                <a href="https://www.linkedin.com/company/careerk-community  " target="_blank" title=""><i class="fa fa-linkedin"></i></a>
                                <a href="https://www.youtube.com/channel/UCTyN3SJ6oqGpg3hJLH_eTSw?view_as=subscriber" target="_blank" title=""><i class="fa fa-youtube"></i></a>
                            </div>
                </div>
                <div class="col-md-3">
<p> &copy; 2019 Careerk <br>
From <i class="fa fa-heart" style="color: red;"> </i> by <a href="http://www.devmokhtar.com/" target="_blank"> Mokhtar Ali</a></p>
                </div>
        </div>
        <a href="#scrollup" class="scrollup" title=""><i class="la la-arrow-up"></i></a>
    </div>
</footer>
</div>


@if (Auth::check() && Auth::user()->role_id == 2)



<div class="profile-sidebar">
        <span class="close-profile"><i class="la la-close"></i></span>
        <div class="can-detail-s">
            <div class="cst"><img src="{{ asset('images/' . Auth::user()->avatar ) }}" alt="" /></div>
            <h3> {{ Auth::user()->name }}</h3>
            <span><i>{{ Auth::user()->name }} </i></span>
            <p><a class="__cf_email__"> {{ Auth::user()->email }} </a></p>
            <p>Member Since, {{ date("M Y" , strtotime(Auth::user()->created_at) ) }}</p>
        </div>
        <div class="tree_widget-sec">
            <ul>


                <li><a href="{{ route('profile.index') }}" title=""><i class="la la-file-text"></i>My Profile</a></li>
                <li><a href="{{ asset('images/' . Auth::user()->cv) }}" target="_blank" title=""><i class="la la-briefcase"></i>My Cv</a></li>
                <li><a href="{{ route("profile.edit" , Auth::user()->id)}}" title=""><i class="la la-file-text"></i> Edit Profile</a></li>
                <li><a href="{{ route('profile.show', Auth::user()->id )}}" title=""><i class="la la-flash"></i>Change Password</a></li>



                <li class="signup-popup">
                    <a title=""href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();"><i class="la la-unlink"></i>Sign Out</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </li>
            </ul>
        </div>
    </div><!-- Profile Sidebar -->


@endif

<script src="{{ asset('website/js/modernizr.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/script.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/wow.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/slick.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/parallax.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/select-chosen.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/counter.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/mouse.js') }}" type="text/javascript"></script>
<script src="{{ asset('website/js/circle-progress.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/plugins/alerts-boxes/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('admin/plugins/alerts-boxes/js/sweet-alert-script.js') }}"></script>

<!--notification js -->
<script src="{{ asset('admin/plugins/notifications/js/lobibox.min.js') }}"></script>
<script src="{{ asset('admin/plugins/notifications/js/notifications.min.js') }}"></script>
<script src="{{ asset('admin/plugins/notifications/js/notification-custom-script.js') }}"></script>



@if(session()->has('message'))

<div class="lobibox-notify-wrapper top right">
    <div class="lobibox-notify lobibox-notify-success animated-fast fadeInDown" style="width: 400px;">
    <div class="lobibox-notify-icon-wrapper">
        <div class="lobibox-notify-icon">
            <div><div class="icon-el"><i class="fa fa-check-circle"></i></div>
        </div></div></div>
        <div class="lobibox-notify-body"><div class="lobibox-notify-title">Success<div></div></div>
        <div class="lobibox-notify-msg" style="max-height: 60px;">{{session()->get('message')}}</div>
    </div><span class="lobibox-close">×</span><div class="lobibox-delay-indicator"><div style="width: 67.3333%;">
</div></div></div>


</div>
@endif

<script>
        $(".lobibox-close").on('click', function () {

            $(this).parent().hide();
        });

    </script>
    
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d1647b8b0926b570bc05105/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>

