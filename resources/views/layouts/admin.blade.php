<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <title>{{ config('app.name', 'careerk') }}</title>
  <link rel="icon" href="{{ asset('/images/logo-icon.png') }}" type="image/x-icon">
  <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/dist/summernote-bs4.css') }}"/>
  <link href="{{ asset('admin/plugins/simplebar/css/simplebar.css') }}" rel="stylesheet"/>
  <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('admin/css/animate.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css"/>
  <link href="{{ asset('admin/css/sidebar-menu.css') }}" rel="stylesheet"/>
  <link rel="stylesheet" href="{{ asset('admin/plugins/notifications/css/lobibox.min.css') }}"/>
  <link href="{{ asset('admin/css/app-style.css') }}" rel="stylesheet"/>
  <script src="{{ asset('admin/js/jquery.min.js') }}"></script>
  <script src="{{ asset('admin/js/popper.min.js') }}"></script>
  <script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/Chart.js/Chart.min.js') }}"></script>
</head>

<body class="bg-theme bg-theme1">

  <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner"><div class="loader"></div></div></div></div>

 <div id="wrapper">

   <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
     <div class="brand-logo">
      <a href="{{ route('dashboard.')}}">

       <img src="{{ asset('images/Careerk-logo-white.png') }}" class="logo-icon" style="width: 195px;margin-top: -15px;" alt="careerk">
     </a>
   </div>
   <div class="user-details">
	  <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
	    <div class="avatar"><img class="mr-3 side-user-img" src="{{ asset('images/' .  Auth()->user()->avatar) }}" alt="user avatar"></div>
	     <div class="media-body">
	     <h6 class="side-user-name">{{ Auth()->user()->name }}</h6>
	    </div>
       </div>
	   <div id="user-dropdown" class="collapse">
		  <ul class="user-setting-menu">
            <li><a href="{{ route('dashboard.profile.index')}}"><i class="icon-user"></i>  My Profile</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="icon-power"></i> {{ __('Logout') }}
             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form></a></li>
		  </ul>
	   </div>
      </div>
   <ul class="sidebar-menu do-nicescrol">
      <li class="sidebar-header">MAIN NAVIGATION</li>
      <li>
        <a href="{{ route('dashboard.')}}" class="waves-effect">
                <i class="icon-home icons"></i> <span>Dashboard</span>
        </a>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
                <i class="icon-layers icons"></i>
          <span> Field </span> <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
		<li><a href="{{ route('dashboard.category.index')}}"><i class="zmdi zmdi-long-arrow-right"></i> View All</a></li>
        <li><a href="{{ route('dashboard.category.create')}}"><i class="zmdi zmdi-long-arrow-right"></i> Add New</a></li>
        </ul>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
                <i class="icon-organization icons"></i>
          <span>Companies</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="{{ route('dashboard.company.index')}}"><i class="zmdi zmdi-long-arrow-right"></i> View All</a></li>
          <li><a href="{{ route('dashboard.company.create')}}"><i class="zmdi zmdi-long-arrow-right"></i> Add New</a></li>
        </ul>
      </li>
      <li>
        <a href="javaScript:void();" class="waves-effect">
                <i class="icon-star icons"></i> <span>Jobs</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="{{ route('dashboard.jobs.index')}}"><i class="zmdi zmdi-long-arrow-right"></i> View All</a></li>
          <li><a href="{{ route('dashboard.jobs.create')}}"><i class="zmdi zmdi-long-arrow-right"></i> Add New</a></li>
        </ul>
       </li>
	   <li>
        <a href="javaScript:void();" class="waves-effect">
                <i class="icon-graduation icons"></i> <span>Students</span>
          <i class="fa fa-angle-left float-right"></i>
        </a>
        <ul class="sidebar-submenu">
          <li><a href="{{ route('dashboard.students')}}"><i class="zmdi zmdi-long-arrow-right"></i> View All</a></li>
        </ul>
      </li>

      <li>
        <a href="{{ route('dashboard.comment.index')}}" class="waves-effect">
          <i class="zmdi zmdi-comment"></i> <span>Comments</span>
        </a>
       </li>

      <li>
        <a href="{{ route('dashboard.apply.index') }}" class="waves-effect">
                <i class="icon-energy icons"></i> <span>Applications</span>
        </a>
      </li>

      <li>

          <a href="{{ route('dashboard.mail.index') }}" class="waves-effect">
            <i class="zmdi zmdi-email"> </i><span>Mailbox</span>
          </a>
        </li>
        <li>
            <a href="javaScript:void();" class="waves-effect">
                    <i class="icon-event icons"></i>
              <span>Careerik Tips</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="sidebar-submenu">
              <li><a href="{{ route('dashboard.blog.index')}}"><i class="zmdi zmdi-long-arrow-right"></i> View All</a></li>
              <li><a href="{{ route('dashboard.blog.create')}}"><i class="zmdi zmdi-long-arrow-right"></i> Add New</a></li>
            </ul>
          </li>

          <li>
                <a href="{{ url('/') }}" class="waves-effect">
                        <i class="icon-eye icons"></i>
                  <span>View Website</span>
                </a>
              </li>
    </ul>

   </div>
   <!--End sidebar-wrapper-->

<!--Start topbar header-->
<header class="topbar-nav">
 <nav class="navbar navbar-expand fixed-top">
  <ul class="navbar-nav mr-auto align-items-center">
    <li class="nav-item">
      <a class="nav-link toggle-menu" href="javascript:void();">
       <i class="icon-menu menu-icon"></i>
     </a>
    </li>
    <li class="nav-item">
      <form class="search-bar">
        <input type="text" class="form-control" placeholder="Enter keywords">
         <a href="javascript:void();"><i class="icon-magnifier"></i></a>
      </form>
    </li>
  </ul>

  <ul class="navbar-nav align-items-center right-nav-link">
  </ul>
</nav>
</header>
<!--End topbar header-->

<div class="clearfix"></div>

@yield('content')


  <script src="{{ asset('admin/js/jquery.min.js') }}"></script>
  <script src="{{ asset('admin/js/popper.min.js') }}"></script>
  <script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/simplebar/js/simplebar.js') }}"></script>
  <script src="{{ asset('admin/js/sidebar-menu.js') }}"></script>
  <script src="{{ asset('admin/js/app-script.js') }}"></script>
  <script src="{{ asset('admin/plugins/peity/jquery.peity.min.js') }}"></script>
  <script src="{{ asset('admin/js/service-support.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
  <script src="{{ asset('admin/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>

    <script>
     $(document).ready(function() {

       var table = $('#example').DataTable( {
        lengthChange: false,
        buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
      } );

     table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

      } );

    </script>
    <script src="{{ asset('admin/plugins/summernote/dist/summernote-bs4.min.js') }}"></script>
    <script>
     $('#summernoteEditor').summernote({
              height: 400,
              tabsize: 2
          });
    </script>
    <!--notification js -->
    <script src="{{ asset('admin/plugins/notifications/js/lobibox.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/notifications/js/notifications.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/notifications/js/notification-custom-script.js') }}"></script>


        @if(session()->has('message'))

        <div class="lobibox-notify-wrapper top right">
            <div class="lobibox-notify lobibox-notify-success animated-fast fadeInDown" style="width: 400px;">
            <div class="lobibox-notify-icon-wrapper">
                <div class="lobibox-notify-icon">
                    <div><div class="icon-el"><i class="fa fa-check-circle"></i></div>
                </div></div></div>
                <div class="lobibox-notify-body"><div class="lobibox-notify-title">Success<div></div></div>
                <div class="lobibox-notify-msg" style="max-height: 60px;">{{session()->get('message')}}</div>
            </div><span class="lobibox-close">×</span><div class="lobibox-delay-indicator"><div style="width: 67.3333%;">
        </div></div></div>


        </div>
        @endif

        <script>
$(".lobibox-close").on('click', function () {

    $(this).parent().hide();
});
        </script>
        <script src="{{ asset('admin/plugins/alerts-boxes/js/sweetalert.min.js') }}"></script>
        <script src="{{ asset('admin/plugins/alerts-boxes/js/sweet-alert-script.js') }}"></script>

        <div class="swal-overlay" tabindex="-1">
                <div class="swal-modal" role="dialog" aria-modal="true"><div class="swal-icon swal-icon--warning">
                  <span class="swal-icon--warning__body">
                    <span class="swal-icon--warning__dot"></span>
                  </span>
                </div><div class="swal-title">Are you sure?</div><div class="swal-text">Once deleted, you will not be able to recover this imaginary file!</div><div class="swal-footer"><div class="swal-button-container">

                  <button class="swal-button swal-button--cancel" tabindex="0">Cancel</button>

                  <div class="swal-button__loader">
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>

                </div><div class="swal-button-container">

                  <button class="swal-button swal-button--confirm swal-button--danger">OK</button>

                  <div class="swal-button__loader">
                    <div></div>
                    <div></div>
                    <div></div>
                  </div>

                </div></div></div></div>


                {{--  <script>

              $('.delete_btn').on("click", function (event) {

                event.preventDefault();

              $(".swal-overlay").addClass("swal-overlay--show-modal");

              });

              $('.swal-button--cancel').on("click", function () {

                $(".swal-overlay").removeClass("swal-overlay--show-modal");

                });

                $('.swal-button--confirm').on("click", function () {

                    $(this).parent('form').submit();

                    });




              </script>  --}}

              <script>

$(".delete_btn").on('click', function() {

      return confirm('Are You Sure You Want To Delete ? ');
});
              </script>

</body>

</html>
