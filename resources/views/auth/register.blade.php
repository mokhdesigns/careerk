@extends('layouts.website')

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
<section class="overlape">
		<div class="block no-padding">
			<div data-velocity="-.1" style="background: url(images/resource/mslider1.jpg) repeat scroll 50% 422.28px transparent;" class="parallax scrolly-invisible no-parallax"></div><!-- PARALLAX BACKGROUND IMAGE -->
			<div class="container fluid">
				<div class="row">
					<div class="col-lg-12">
						<div class="inner-header wform">
							<div class="job-search-sec">
								<div class="job-search">
                                    <h4> Create New Account   </h4>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<section>


	<section style="background: url(images/bg.jpg);background-attachment: fixed;background-size: cover;background-repeat: no-repeat;">
		<div class="block no-padding">
			<div class="container">
				 <div class="row no-gape">

				 	<div class="col-lg-12 column">
				 		<div class="padding-left">
					 		<div class="manage-jobs-sec">

						 		<div class="coverletter-sec">

                                        <form method="POST" action="{{ route('register') }}" style="min-width: 100%;" enctype="multipart/form-data">
                                        @csrf

						 				<div class="row">
						 					<div class="col-lg-6">
						 						<span class="pf-title">Your Name </span>
						 						<div class="pf-field">
						 							<input type="text" value="{{ old('name') }}" required name="name">
						 						</div>
                                             </div>

						 					<div class="col-lg-6">
                                                <span class="pf-title">Your Phone </span>
                                                <div class="pf-field">
                                                    <input type="text" value="{{ old('phone') }}" required name="phone">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Email </span>
                                                <div class="pf-field">
                                                    <input type="email" value="{{ old('email') }}" required name="email">
                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Career </span>
                                                <div class="pf-field">
                                                    <input type="text" value="{{ old('career') }}" required name="career">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                    <span class="pf-title">New Password </span>
                                                    <div class="pf-field">
                                                        <input type="password"  required name="password">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                        <span class="pf-title">Password Confirm </span>
                                                        <div class="pf-field">
                                                            <input type="password"  required name="password_confirmation">
                                                        </div>
                                                    </div>
                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Cv </span>
                                                <div class="pf-field">
                                                    <input type="file" required   name="cv" required="required">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Photo </span>
                                                <div class="pf-field">
                                                    <input type="file" required  name="avatar"  required="required">
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <span class="pf-title">Your Gender</span>
                                                <div class="pf-field">
                                                    <select data-placeholder="Select Your gender" class="chosen" name="gender">
                                                       <option value="0">Male</option>
                                                       <option value="1">Female</option>
                                                   </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <span class="pf-title">About You</span>
                                                <div class="pf-field">
                                                    <textarea required name="body">{{ old('body') }}</textarea>
                                                </div>
                                            </div>

						 					<div class="col-lg-6">
						 						<span class="pf-title">Your Education</span>
						 						<div class="pf-field">
						 							<textarea required name="education"> {{ old('education') }} </textarea>
						 						</div>
                                             </div>

                                             <div class="col-lg-6">
                                                <span class="pf-title">Your experience</span>
                                                <div class="pf-field">
                                                    <textarea required name="experience"> {{ old('experience') }} </textarea>
                                                </div>
                                            </div>

						 					<div class="col-lg-12">
						 						<button type="submit">Register</button>
						 					</div>
						 				</div>
						 			</form>
						 		</div>
					 		</div>
					 	</div>
					</div>
				 </div>
			</div>
		</div>
	</section>

</div>


@include('partials._errors')
@endsection
