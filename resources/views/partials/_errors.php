<?php
if ($errors->any()){ ?>

    <div class="lobibox-notify-wrapper top right">
<?php 
        foreach ($errors->all() as $error){ ?>


    <div class="lobibox-notify lobibox-notify-warning animated-fast fadeInDown notify-mini rounded" style="width: 400px;">
    <div class="lobibox-notify-icon-wrapper">
        <div class="lobibox-notify-icon"><div>
            <div class="icon-el"><i class="fa fa-exclamation-circle"></i></div>
        </div></div></div><div class="lobibox-notify-body">
            <div class="lobibox-notify-msg" style="max-height: 32px;"><?php echo $error ?></div></div><span class="lobibox-close">×</span></div>
       

            <?php   } ?>
            </div>
<?php 

} ?>

<script>

$(".lobibox-close").on('click', function() {

    $(this).parent().hide();
});
</script>

