<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'name'     => 'Super Admin',
            'email'    => 'admin@app.com',
            'password' => bcrypt('secret'),
            'avatar'   => 'default.png',
            'role_id'  => '1',


        ]);
    }
}
