<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([

            'title'  => 'Education Training',
            'avatar' => '<i class="la la-graduation-cap"> </i>'
        ]);


        Category::create([

            'title'  => 'Web Companies',
            'avatar' => '<i class="la la-graduation-cap"> </i>'
        ]);


        Category::create([

            'title'  => 'Accounting And Finance',
            'avatar' => '<i class="la la-bullhorn"> </i>'
        ]);

        Category::create([

            'title'  => 'Human Resources',
            'avatar' => '<i class="la la-users"> </i>'
        ]);
}
}
