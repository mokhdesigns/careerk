<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('position');
            $table->string('experience');
            $table->boolean('paid');
            $table->string('type');
            $table->string('avatar');
            $table->longText('body');
            $table->timestamps();
            $table->integer('category_id')->unsigned();
            $table->integer('company_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
