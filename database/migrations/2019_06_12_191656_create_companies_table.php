<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('avatar');
            $table->string('email')->nullable();
            $table->string('mobile')->nullable();
            $table->string('facebook');
            $table->string('linkedin');
            $table->string('twitter');
            $table->string('location');
            $table->longText('body');
            $table->integer('views')->default(0);
            $table->integer('category_id')->unsigned();
            $table->timestamps();


            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
