<?php


//====================================================================================================
//
//    ###    ####    ###    ###  ##  ##     ##        #####     #####   ##   ##  ######  #####   ####
//   ## ##   ##  ##  ## #  # ##  ##  ####   ##        ##  ##   ##   ##  ##   ##    ##    ##     ##
//  ##   ##  ##  ##  ##  ##  ##  ##  ##  ## ##        #####    ##   ##  ##   ##    ##    #####   ###
//  #######  ##  ##  ##      ##  ##  ##    ###        ##  ##   ##   ##  ##   ##    ##    ##        ##
//  ##   ##  ####    ##      ##  ##  ##     ##        ##   ##   #####    #####     ##    #####  ####
//
//=====================================================================================================

Route::prefix('dashboard')->name('dashboard.')->middleware(['auth','Admin'])->group(function () {

Route::get('/', 'DashboardController@index');

Route::resource('/comment', 'CommentController');

Route::resource('/category', 'CategoriesController');

Route::resource('/company', 'CompaniesController');

Route::resource('/jobs', 'JobsController');

Route::get('/students', 'DashboardController@students')->name('students');

Route::resource('apply', 'ApplyController');

Route::resource('/blog', 'BlogController');

Route::resource('/mail', 'ContactController');

Route::resource('profile', 'ProfileController');



});
