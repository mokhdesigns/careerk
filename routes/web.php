<?php


//====================================================================================
//
//  ##      ##  #####  #####         #####     #####   ##   ##  ######  #####   ####
//  ##      ##  ##     ##  ##        ##  ##   ##   ##  ##   ##    ##    ##     ##
//  ##  ##  ##  #####  #####         #####    ##   ##  ##   ##    ##    #####   ###
//  ##  ##  ##  ##     ##  ##        ##  ##   ##   ##  ##   ##    ##    ##        ##
//   ###  ###   #####  #####         ##   ##   #####    #####     ##    #####  ####
//
//====================================================================================



//=======================================================
//
//   ####    ###    #####    #####  #####  #####    ##  ##
//  ##      ## ##   ##  ##   ##     ##     ##  ##   ## ##
//  ##     ##   ##  #####    #####  #####  #####    ####
//  ##     #######  ##  ##   ##     ##     ##  ##   ## ##
//   ####  ##   ##  ##   ##  #####  #####  ##   ##  ##  ##
//
//=========================================================




Route::get('/', 'Website\HomeController@index');

Route::resource('contact', 'Website\ContactController');

Route::get('/about', 'Website\HomeController@about');

Route::resource('blog', 'Website\BlogController');

Route::resource('job', 'Website\JobController');

Route::resource('profile', 'Website\ProfileController')->middleware(['auth', 'Student']);

Route::get('\profile/changePassword', function() {


    dd('this is right');
});

Route::get('/profile@changePassword', 'Website\ProfileController@changPassword')->middleware(['auth', 'Student']);

Route::resource('candidates', 'Website\CandidatesController');

Route::resource('company', 'Website\CompanyController');

Route::resource('category', 'Website\CategoryController');

Route::resource('apply', 'Website\ApplyController')->middleware(['auth', 'Student']);

Route::post('search', 'Website\ApplyController@search')->name('search');

Route::resource('comment', 'Website\CommentController');

Auth::routes();
